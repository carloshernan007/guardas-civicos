<?php
/**
* Contralador: Gestiona todos los campos de localizacion de los usuarios
* Fecha: 02 de marzo del 2010
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Localizacion extends Controller {

	function Localizacion()
	{
		parent::Controller();	
		
	}
/******************************************************************************************
  Accion que presenta la lista  y administracion de departamentos
*******************************************************************************************/
   function index ()
    {
		$this->load->database ();
		$this->load->model('camposRegistro');
		$dato['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$dato['departamento'] = $this->camposRegistro->departamentos ();
		$this->db->close ();
		$this->load->view('administrador/header',$dato);
		$this->load->view('localizacion/inicio');
		$this->load->view('footer');	
    }
/******************************************************************************************
  Accion que lista las cidades de un departamento
******************************************************************************************/
  function ciudad($id = 0)
   {
	 if($id != 0 )
	  { 
	    $this->load->database ();
	    $this->load->model('camposRegistro');
		$this->load->model('camposRegistro');
		$dato['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $dato['ciudad'] = $this->camposRegistro->ciudad ($id);
	    $dato['departamento'] = $this->camposRegistro->nombreDepartamento($id);
	    $this->db->close ();  
		$this->load->view('administrador/header',$dato);
		$this->load->view('localizacion/ciudad',$dato);
		$this->load->view('footer');	
	  }
	 else
	  {
		redirect('actividad/salir');  
	  }
   }
/********************************************************************************************
 Accion que  genera la interfaz el nombre el nombre de un departamento
********************************************************************************************/
  function editaDepartamento($id= 0)
   {
	  if($id != 0)
	   {
		$this->load->database ();
	    $this->load->model('camposRegistro');
		$dato['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $dato['departamento'] = $this->camposRegistro->nombreDepartamento($id);
	    $this->db->close ();  
		$this->load->view('administrador/header',$dato);
		$this->load->view('localizacion/editaDepartamento');
		$this->load->view('footer');	 
	   }
	   else
		redirect('actividad/salir');  
   }
/********************************************************************************************
 Accion que actualiza el departamento
********************************************************************************************/
 function actualizaDepartamento()
  {
	 extract($_POST);
	 if(isset($id) != false)
	  {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->actualizaDepartamento($id,$nombre);
	    $this->db->close ();   
		if($dato == false)
		 $mensaje = 'No se pudo actualizar el nombre del departamento verifique que el nombre no exista';
		else
		 $mensaje = "Se actualizo el depatamento $nombre";
		 $this->session->set_flashdata('mensaje', $mensaje);
		 redirect('localizacion');  
      }
	 else
	  redirect('actividad/salir');  
  }
/********************************************************************************************
 Accion que elimina un departamento ()
********************************************************************************************/
 function eliminarDepartamento ($id=0)
  {
	if($id != 0)
	 {
		$this->load->database ();
	    $this->load->model('registroFormulario');
		
	    $dato = $this->registroFormulario->eliminaDepartamento($id);
	    $this->db->close (); 
		if($dato == false)
		 $mensaje = 'No se puede eliminar el departamento, verifique que este no tiene ciudades asignadas';
		else
		 $mensaje = "El departamento se ha eliminado correctamente";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect('localizacion');
	 }
	 else
	  redirect('actividad/salir');  
  }
/*******************************************************************************************
 Accion que crear un nuevo departamento
*******************************************************************************************/
function addDepartamento ()
 {
	$this->load->database ();
	$this->load->model('camposRegistro');
	$dato['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	$this->db->close ();   
	$this->load->view('administrador/header',$dato);
    $this->load->view('localizacion/addDepartamento');
    $this->load->view('footer'); 
 }
/******************************************************************************************
 Accion que registra el nuevo departamento 
******************************************************************************************/
function registraDepartamento ()
 {
    extract($_POST);
	if(isset($nombre) != false)
	 {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->nuevoDepartamento($nombre);
	    $this->db->close (); 
		if($dato == false)
		 $mensaje = "No se ingreso el departamento $nombre por que ya existe!";
		else
		 $mensaje = "Se ingreso un nuevo departamento correctamente";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect('localizacion');
	 }
	else
	 redirect('actividad/salir');
 }
/********************************************************************************************
Accion que presenta de todos los  barrios de una ciudad
********************************************************************************************/
function barrio ($id = 0)
 {
	  
    if($id != 0)
	 {
		extract($_POST); 
		$this->load->database ();
	    $this->load->model('camposRegistro');
	    $dato['ciudad'] = $this->camposRegistro->informacionCiudad($id);
		$dato['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$dato['id'] = $id;
		if(isset($comuna) == false)
		$dato['barrio'] = $this->camposRegistro->barrio($id);
		else
		 {
		   if($comuna != 0 )	 
		   $dato['barrio'] = $this->camposRegistro->barrioFiltro($id,$comuna);
		   else
		   $dato['barrio'] = $this->camposRegistro->barrio($id);
		 }
	    $this->db->close ();   
		$this->load->view('administrador/header',$dato);
        $this->load->view('localizacion/barrio',$dato);
        $this->load->view('footer');  
	 }
	else
	 redirect('actividad/salir');
 }
/*******************************************************************************************
Accion que genera la interfaz una nueva ciudad
********************************************************************************************/
function addCiudad ($id=0)
 {
	if($id==0)
	 {
		$this->load->database ();
	    $this->load->model('camposRegistro');
	    $dato['departamento'] = $this->camposRegistro->departamentos();
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $this->db->close ();   
	 }
	else
	  $dato['departamento'] = false;
	  $dato['id'] = $id;
	$this->load->view('administrador/header',$info);
    $this->load->view('localizacion/addCiudad',$dato);
    $this->load->view('footer');  
 }
/*******************************************************************************************
 Accion que registra la nueva ciudad
*******************************************************************************************/
function registraCiudad ()
 {
    extract($_POST);
	if(isset($nombre) != false)
	 {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->nuevaCiudad($nombre,$departamento);
	    $this->db->close ();  
		if($dato == false)
		$mensaje = "No se ingreso la nueva  ciudad $nombre por que ya existe!";
		else
		 $mensaje = "Se ingreso una nueva ciudad  correctamente";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/ciudad/$departamento");
	 }
	else 
    redirect('actividad/salir');
 }
/*******************************************************************************************
 Accion que elimina una ciudad
********************************************************************************************/
function eliminarCiudad($id=0,$departamento=0)
{
   if($id != 0)
    {
	    $this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->eliminaCiudad($id);
	    $this->db->close (); 
		if($dato == false)
		 $mensaje = 'No se puede eliminar la ciudad, verifique que no tenga barrios o corregimientos';
		else
		 $mensaje = "la ciudad fue eliminada";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/ciudad/$departamento");
	 }
	 else
	  redirect('actividad/salir');  
}
/********************************************************************************************
 Accion que permite genera la  interfas para editar una ciudad
********************************************************************************************/
function editaCiudad($id=0)
{
  	if($id!=0)
	 {
		$this->load->database ();
	    $this->load->model('camposRegistro');
	    $dato['departamento'] = $this->camposRegistro->departamentos();
		$dato['ciudad'] = $this->camposRegistro->informacionCiudad($id);
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $this->db->close ();   
		$this->load->view('administrador/header',$info);
        $this->load->view('localizacion/editaCiudad',$dato);
         $this->load->view('footer'); 	
	 }
	else
	  redirect('actividad/salir');
	
}
/*******************************************************************************************
   Accion que actualiza la ciudad
*******************************************************************************************/
function actualizaCiudad()
 {
	extract($_POST);
	if(isset($nombre) != false)
	 {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->actualizaCiudad($nombre,$departamento,$id);
	    $this->db->close ();  
		if($dato == false)
		$mensaje = "No se pudo actualizar la  ciudad $nombre !";
		else
		 $mensaje = "Se actualizo la  ciudad  $nombre";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/ciudad/$departamento"); 
     }
 }
/********************************************************************************************
  Accion que genera la interfas del para la actualziacion del barrio
********************************************************************************************/
function editaBarrio($id=0,$departamento=0)
 {
   if($id != false)
    {
	    $this->load->database ();
	    $this->load->model('camposRegistro');
	    $dato['ciudad'] = $this->camposRegistro->ciudad ($departamento);
		$dato['barrio'] = $this->camposRegistro->informacionBarrio($id);
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $this->db->close ();  
		$this->load->view('administrador/header',$info);
        $this->load->view('localizacion/editaBarrio',$dato);
        $this->load->view('footer'); 	
    }
   else
     redirect('actividad/salir');
 }
/********************************************************************************************
  Accion que elimina un barrio
********************************************************************************************/
function eliminaBarrio ($id=0,$ciudad)
 {
   if($id != false)
    {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->eliminaBarrio($id);
	    $this->db->close (); 
		if($dato == false)
		 $mensaje = 'No se puede eliminar el barrio por que existen usuarios registrados con el ';
		else
		 $mensaje = "el barrio fue eliminado";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/barrio/$ciudad");
	}
   else	
    redirect('actividad/salir');
 }
/********************************************************************************************
 Accion que actualiza  la informacion sobre un barrio
********************************************************************************************/
function actualizaBarrio ()
 {
   extract($_POST);
   if(isset($ciudadActual) != false)
    {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->actualizaBarrio($nombre,$comuna,$ciudad,$id);
	    $this->db->close ();  
		if($dato == false)
		$mensaje = "No se pudo actualizar el barrio  $nombre !";
		else
		 $mensaje = "Se actualizo el barrio   $nombre";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/barrio/$ciudadActual"); 
		
    }
    else
	  redirect('actividad/salir');
 }
/********************************************************************************************
 Accion que genera la interfas  un nuevo barrio
********************************************************************************************/
function addBarrio ($departamento=0,$ciudad=0)
 {
   $this->load->database ();	 
   $this->load->model('camposRegistro');
   $dato['ciudad'] = $this->camposRegistro->ciudad($departamento);
   $dato['ciudadActual'] = $this->camposRegistro->informacionCiudad($ciudad);
   $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
   $this->db->close ();
   $this->load->view('administrador/header',$info);
   $this->load->view('localizacion/addBarrio',$dato);
   $this->load->view('footer'); 	 
 }
/*******************************************************************************************
 Accion que registra los barrios 
*******************************************************************************************/
function registraBarrio()
 {
   extract($_POST);
   if(isset($nombre) != false)
	 {
		$this->load->database ();
	    $this->load->model('registroFormulario');
		$error = 0;
		foreach($nombre as $barrio)
         {		
	      $dato = $this->registroFormulario->nuevaBarrio($barrio,$comuna,$ciudad);
		  if($dato == false)
		   $error++;
		 }
	    $this->db->close ();  
		if( $error > 0)
		$mensaje = "Uno o mas barrios no se registraron por que ya existen!";
		else
		 $mensaje = "Se registraron los nuevos barrios";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/barrio/$ciudad");
	 }
	else 
    redirect('actividad/salir');
 }
/*******************************************************************************************
 Accion que lista los corregimiento de una ciudad
*******************************************************************************************/
function corregimiento($id=0)
 {
   if($id != 0)
    {
	   $this->load->database ();	 
	   $this->load->model('camposRegistro');
	   $dato['corregimiento'] = $this->camposRegistro->corregimiento($id);
	   $dato['ciudad'] = $this->camposRegistro->informacionCiudad($id);
	   $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	   $this->db->close ();
	   $this->load->view('administrador/header',$info);
	   $this->load->view('localizacion/corregimiento',$dato);
	   $this->load->view('footer'); 	
	}
   else 
    redirect('actividad/salir');	
 }
/*******************************************************************************************
 Accion que  genera la interfaz de  nuevo corregimiento
*******************************************************************************************/
function addCorregimiento ($departamento=0, $ciudad=0)
 {
   if($departamento != 0)
    {
	   $this->load->database ();	 
	   $this->load->model('camposRegistro');
	   $dato['ciudad'] = $this->camposRegistro->ciudad($departamento);
	   $dato['ciudadActual'] = $this->camposRegistro->informacionCiudad($ciudad);
	   $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	   $this->db->close ();
	   $this->load->view('administrador/header',$info);
	   $this->load->view('localizacion/addCorregimiento',$dato);
	   $this->load->view('footer'); 	  
	}
   else 
    redirect('actividad/salir');		
 }
/*******************************************************************************************
 Accion que  Registra los corregimientos
*******************************************************************************************/
function registraCorregimiento ()
 {
	extract($_POST);
   if(isset($nombre) != false)
	 {
		$this->load->database ();
	    $this->load->model('registroFormulario');
		$error = 0;
		foreach($nombre as $corregimiento)
         {		
	      $dato = $this->registroFormulario->nuevoCorregimiento($corregimiento,$ciudad);
		  if($dato == false)
		   $error++;
		 }
	    $this->db->close ();  
		if( $error > 0)
		$mensaje = "Uno o mas Corregimientos no se registraron por que ya existen!";
		else
		 $mensaje = "Se registraron los nuevos Corregimientos";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/Corregimiento/$ciudad");
	 }
	else 
    redirect('actividad/salir'); 
 }
/*******************************************************************************************
 Accion que elimina un corregimiento
*******************************************************************************************/
function eliminaCorregimiento($id=0,$ciudad)
 {
	  if($id != 0)
    {
		$this->load->database ();
	    $this->load->model('registroFormulario');
	    $dato = $this->registroFormulario->eliminaCorregimiento($id);
	    $this->db->close (); 
		if($dato == false)
		 $mensaje = 'No se puede eliminar el Corregimiento por que existen veredas registradas con el ';
		else
		 $mensaje = "el corregimiento fue eliminado";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/corregimiento/$ciudad");
	}
   else	
    redirect('actividad/salir');
 }
/********************************************************************************************
 Accion que  genera la interfas para editar el corregimiento
********************************************************************************************/
function editaCorregimiento($id,$departamento)
 {
	 if($id != 0)
	  {
		$this->load->database ();
	    $this->load->model('camposRegistro');
	    $dato['ciudad'] = $this->camposRegistro->ciudad($departamento);
		$dato['corregimiento'] = $this->camposRegistro->informacionCorregimiento($id);
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $this->db->close ();  
		$this->load->view('administrador/header',$info);
        $this->load->view('localizacion/editaCorregimiento',$dato);
        $this->load->view('footer'); 	  
	  }
	 else	
      redirect('actividad/salir'); 
 }
/*******************************************************************************************
 Accion que actualiza los datos de un corregimiento
*******************************************************************************************/
function actualizaCorregimiento ()
 {
   extract($_POST);
   if(isset($id) != false)
    {
	  $this->load->database ();
	  $this->load->model('registroFormulario');
	  $dato = $this->registroFormulario->actualizaCorregimiento($ciudad,$nombre,$id);
	  $this->db->close (); 	
	  if($dato == false)
		 $mensaje = 'No se puede actualizar el corregimiento $nombre por que ya exite! ';
		else
		 $mensaje = "el corregimiento $nombre fue actualizado";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/corregimiento/$ciudad");
	}
   else
     redirect('actividad/salir'); 
 }
/******************************************************************************************
 Accion que genera interdas con todos las veredas de un corregimiento
******************************************************************************************/
function vereda($id=0)
 {
 	if($id != 0)
	 {
		$this->load->database ();
		$this->load->model('camposRegistro');
		$corregimiento = $this->camposRegistro->informacionCorregimiento($id);
		$dato = array(
					   'vereda' => $this->camposRegistro->veredas($id),
					   'corregimiento' => $corregimiento,
					   'titulo' => "Lista de veredas del corregimiento $corregimiento->nombre"
					  );
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$this->db->close ();
		$this->load->view('administrador/header',$info);
        $this->load->view('localizacion/vereda',$dato);
        $this->load->view('footer');  
	 }
	else
     redirect('actividad/salir');   
 }
/****************************************************************************************
Accion que genera la interfas para una nueva vereda
****************************************************************************************/
function addVereda($ciudad=0,$corregimiento)
 {
	 if($ciudad != 0 )
	  {
		$this->load->database ();
	    $this->load->model('camposRegistro');
	    $dato['corregimientos'] = $this->camposRegistro->corregimiento($ciudad);
		$dato['corregimiento'] = $this->camposRegistro->informacionCorregimiento($corregimiento);
		$dato['titulo'] = 'Ingresando nuevas veredas';
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	    $this->db->close ();    
		$this->load->view('administrador/header',$info);
        $this->load->view('localizacion/addVereda',$dato);
        $this->load->view('footer');  
	  }
	 else
     redirect('actividad/salir');   
 }
/***************************************************************************************
 Accion que realiza el registro de las nuevas veredas
***************************************************************************************/
function registraVereda ()
 {
   extract($_POST);
   if(isset($nombre) != false)
    {
	   $this->load->database ();
	    $this->load->model('registroFormulario');
		$error = 0;
		foreach($nombre as $registro)
         {		
	      $dato = $this->registroFormulario->registraVereda($corregimiento,$registro);
		  if($dato == false)
		   $error++;
		 }
	    $this->db->close ();  
		if( $error > 0)
		$mensaje = "Uno o mas veredas no se registraron por que el nombre ya existe!";
		else
		 $mensaje = "Se registraron los nuevas veredas";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/vereda/$corregimiento");	
	}
   else
     redirect('actividad/salir');   	
 }
/***************************************************************************************
 Accion que genera la interfas para editar una vereda
***************************************************************************************/
function editaVereda ($id=0,$ciudad)
 {
   if($id != 0)
    {
		$this->load->database ();
	    $this->load->model('camposRegistro');
		$dato = array(
					   'vereda' => $this->camposRegistro->informacionVereda($id),
					   'corregimiento' => $this->camposRegistro->corregimiento($ciudad),
					   'titulo' => 'Editando la informacion de la vereda'
					  );
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$this->load->view('administrador/header',$info);
        $this->load->view('localizacion/editaVereda',$dato);
        $this->load->view('footer');  
		$this->db->close();
	}
   else
     redirect('actividad/salir');   	
 }
/************************************************************************************
 Accion que actualiza el registro de una vereda
*************************************************************************************/
function actualizaVereda ()
 {
    extract($_POST);
	if(isset($id) != false)
	 {
	  $this->load->database ();
	  $this->load->model('registroFormulario');
	  $dato = $this->registroFormulario->actualizaVereda($corregimiento,$nombre,$id);
	  $this->db->close (); 	
	  if($dato == false)
		 $mensaje = 'No se puede actualizar la vereda $nombre por que ya exite! ';
		else
		 $mensaje = "la vereda $nombre fue actualizado";
		 $this->session->set_flashdata('mensaje', $mensaje); 
		  redirect("localizacion/vereda/$corregimiento"); 
	 }
	 else
     redirect('actividad/salir');    
 }
/**************************************************************************************
 Accion que elimina una vereda 
**************************************************************************************/
function eliminarVereda($id=0,$corregimiento)
{
  if($id != 0)
   {
	  $this->load->database ();
	  $this->load->model('registroFormulario');
	  $dato = $this->registroFormulario->eliminaVereda($id);
	  $this->db->close (); 
	  if($dato == false)
	   $mensaje = 'No se puede eliminar la vereda  por que existen usuarios asignados a esta ! ';
	  else
	   $mensaje = "la vereda fue eliminada";
	   $this->session->set_flashdata('mensaje', $mensaje); 
	   redirect("localizacion/vereda/$corregimiento");   
	   
   }
  else
   redirect('actividad/salir');  
}
 
}
?>
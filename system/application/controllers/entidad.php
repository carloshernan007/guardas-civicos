<?php
/**
* Contralador: Gestion todo lo referente al entidades 
* Fecha: 10 de marzo del 2010
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Entidad extends Controller {

	function Entidad ()
	{
		parent::Controller();	
	}

/*******************************************************************************************
 Metodo que sierra la seccion
*******************************************************************************************/
   function index ()
    {
	  extract($_POST);
	  if(isset($departamento) == false ){		  
	    $departamento = 0;
		$ciudades = 0;
		$sector = -1;
	  }
	  
	  $this->load->database ();
	  $this->load->model('camposRegistro');
	  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	  
	  $dato = array ( 'titulo' => 'Gestor de entidades',
					  'departamento' => $this->camposRegistro->departamentos (),
					  'entidades' => $this->camposRegistro->entidades ($departamento,$ciudades,$sector),
					  'd' => $departamento,
					  'c' => $ciudades,
					  'ciudad' => $this->camposRegistro->ciudad($departamento)
					 );
	  
	  $this->db->close ();
	  $this->load->view('administrador/header',$info);
	  $this->load->view('entidad/inicio',$dato);
	  $this->load->view('footer');
	
	}
/***************************************************************************************
 Accion que genera la inferfas para ingresar entidades 
***************************************************************************************/
  function add ()
   {
	 $this->load->database ();
	  $this->load->model('camposRegistro');
	  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	  
	  $dato = array ( 'titulo' => 'Registranto entidades',
					  'departamento' => $this->camposRegistro->departamentos (),
					 );
	  
	  $this->db->close ();
	  $this->load->view('administrador/header',$info);
	  $this->load->view('entidad/add',$dato);
	  $this->load->view('footer');  
   }
/*****************************************************************************************
 Accion que registra las nuevas entidades
*****************************************************************************************/
  function registrar ()
  {
	 extract($_POST);
	 if(isset($nombre) != false)
	  {
       $this->load->database ();
	   $this->load->model('registroFormulario');
	   $fecha =date("Y-m-d h:i:s ");
	   $error = 0;
	    foreach($nombre as $registro)
		 {
		   $dato = $this->registroFormulario->nuevaEntidad($ciudad,$sector,$registro,$fecha);	 
		   if($dato == false)
		    $error++;
		 }
	   if($error > 0)
		 $mensaje = 'No se pudieron registrar algunas entidades por que el nombre ya existe!';
		else
		 $mensaje = "Se registraron las nuevas entidades";
		 $this->session->set_flashdata('mensaje', $mensaje);	 
	   $this->db->close ();
	   redirect('entidad');  
	 }
	 else
		redirect('actividad/salir');   
  }
/******************************************************************************************
 Accion que edita una entidad
******************************************************************************************/
function editar($id = 0)
 {
      if($id != 0)
	   {
		  $this->load->database ();
		  $this->load->model('camposRegistro');
		  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		  
		  $dato = array ( 'titulo' => 'Editando Entidad',
						  'departamento' => $this->camposRegistro->departamentos (),
						  'entidad' => $this->camposRegistro->informacionEntidad ($id),
						 );
		  
		  $this->db->close ();
		  $this->load->view('administrador/header',$info);
		  $this->load->view('entidad/editar',$dato);
		  $this->load->view('footer');  	 
	   }
	  else
		redirect('actividad/salir');   
 }
/******************************************************************************************
 Accion que realiza la actualizacion la informacion de una ectidad
******************************************************************************************/
function actualizar ()
 {
    extract($_POST);
	if(isset($id) != false)
	 {
	   $this->load->database ();
	   $this->load->model('registroFormulario');
	   $this->registroFormulario->actualizaEntidad($nombre,$ciudad,$sector,$departamento,$id);
	   $this->db->close ();
	   $mensaje = "Se actualizo la entidad $nombre";
	   $this->session->set_flashdata('mensaje', $mensaje);	
	   redirect('entidad');   
	 }
	else 
	 redirect('actividad/salir');   	  
 }
/*****************************************************************************************
 Accion que elimina una entidad
*****************************************************************************************/
function eliminar($id=0)
{
  if($id != 0)
    {
	  $this->load->database ();
	  $this->load->model('registroFormulario');	
	  $error = $this->registroFormulario->eliminaEntidad($id);
	  $this->db->close ();
	  if($error == false)
		 $mensaje = 'No se puede eliminar la entidad por que esta contiene registrados eventos!';
		else
		 $mensaje = "Se elimino al entidad ";
	  $this->session->set_flashdata('mensaje', $mensaje); 
	  redirect('entidad'); 
	}
  else
   redirect('actividad/salir'); 
	
}
}

?>
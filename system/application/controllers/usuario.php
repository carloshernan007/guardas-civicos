<?php
/**
* Contralador: Gestiona las acciones sobre los usuarios registrados, eliminar, buscar, modificar
* Fecha: 02 de marzon del 2008
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Usuario extends Controller {

	function Usuario()
	{
		parent::Controller();	
	}
/******************************************************************************************
    Accion que  presenta la interfaz de busqueda de usuarios
******************************************************************************************/
   function index ()
    {
	  $this->load->database ();
	  $this->load->model('camposRegistro');
	  $dato['departamento'] = $this->camposRegistro->departamentos ();
	  $dato['perfil'] = $this->camposRegistro->perfil();
	  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	  $this->db->close ();
	  $this->load->view('administrador/header',$info);
	  $this->load->view('usuario/inicio',$dato);
	  $this->load->view('footer');
    }
/******************************************************************************************
  Accion que realiza la busqueda de los usuarios e imprime los resultados
*******************************************************************************************/
   function buscarUsuario ()
    {
	  extract($_POST);	
	  $this->load->database ();
	  $this->load->model('consultas');
	   $this->load->model('camposRegistro');
	  $dato['registros'] = $this->consultas->buscarUsuario($departamento,$ciudad,$perfil,$nombre,$apellido,$contrato);
	  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	  $this->db->close ();	
	  $this->load->view('administrador/header',$info);
	  $this->load->view('usuario/buscarUsuario',$dato);
	  $this->load->view('footer');	
    }
/*******************************************************************************************
 Accion que genera la informacion sobre un  usuario 
*******************************************************************************************/
  function informacionUsuario($id=0)
   {
	 if($id != 0)
	  {
	    $this->load->database ();
	    $this->load->model('consultas');  
		$this->load->model('camposRegistro');
		$this->load->library('funciones');
		$user = $this->consultas->informacionPersonal ($id);
		$user->nivel_academico = $this->funciones->nivel_academico($user->nivel_academico);
		$user->fecha_nacimiento = $this->funciones->edad($user->fecha_nacimiento); 
		$dato['pariente'] = $this->consultas->informacionPariente($id);
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$dato['escolaridad'] = $this->camposRegistro->escolaridad();
		$dato['user'] = $user;
		$dato['id'] = $id;
		$this->load->view('administrador/header',$info);
	    $this->load->view('usuario/informacionUsuario',$dato);
	    $this->load->view('footer');	
	  }
	 else
	  {
		redirect('welcome');  
	  }
   }
/******************************************************************************************
 Accion que registra un nuevo familiar
******************************************************************************************/
  function addFamiliar ()
   {
	  extract($_POST);
	  if(isset($nombre))
	   {
		   $registro = 1;
		    $fecha =date("Y-m-d h:i:s ");
		   $this->load->database ();
		   $this->load->model('registroFormulario');
		   $this->registroFormulario->addFamiliar(
												  $usuario,
												  $registro,
												  $nombre,
												  $apellido,
												  $fechaNacimiento,
												  $parentesco,
												  $aptitudDeportiva,
												  $aptitudArtistica,
												  $nivelAcademico,
												  $fecha,
												  $telefono
												  );
		   $this->db->close ();
		   redirect($url);
	   }
	  else
	   {
		   redirect('welcome');  
	   }
   }

 
}
?>
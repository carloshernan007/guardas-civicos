<?php
/**
* Contralador: Gestiona las acciones validas solo para el usuario administrador
* Fecha: 23 de febrero del 2010
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Administrador extends Controller {

	function Administrador()
	{
		parent::Controller();	
	}
/******************************************************************************************
 Accion que genera la interfaz de inicio del administrador
******************************************************************************************/
	function index()
	{
		$this->load->database ();
		$this->load->model('camposRegistro');
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$this->db->close ();
		$this->load->view('administrador/header',$info);
		$this->load->view('administrador/inicio');
		$this->load->view('footer');
	}
/*************************************************************************************************
 Accion que genera la  interfaz para la administracion de la configuracion global de la aplicacion
**************************************************************************************************/
   function configuracion ()
    {
		$this->load->database ();
		$this->load->model('camposRegistro');
		$dato['perfil'] = $this->camposRegistro->perfil ();
		$dato['estado'] = $this->camposRegistro->estadoConfiguracion ();
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$this->db->close ();
	    $this->load->view('administrador/header',$info);
		$this->load->view('administrador/configuracion',$dato);
		$this->load->view('footer');	
	}
/*************************************************************************************************
 Accion que actualiza la configuracion del sitio
*************************************************************************************************/
  function actualizarSitio ()
   {
	  extract($_POST);
	  if(isset($nombre) != false)
	   {
	     $this->load->database ();
	     $this->load->model('registroFormulario');
		 $this->registroFormulario->configuracionGlobal($nombre,$nivelAcceso,$email);
		 $this->db->close (); 
		  $this->session->set_flashdata('mensaje', 'Se realizo al actualizaci&oacute;n'); 
	   }
	   redirect('administrador/configuracion');
   }
/*************************************************************************************************
 Accion que cambia el estado de registro de usuario
*************************************************************************************************/
  function registro ($id=1)
   {
	 $this->load->database ();
	 $this->load->model('registroFormulario');
	 $this->registroFormulario-> permisos('registro_usuarios ',$id);
	 $this->db->close ();
	 redirect('administrador/configuracion');
   }
/*************************************************************************************************
 Accion que cambia el estado de sitio
*************************************************************************************************/
  function sitio ($id=1)
   {
	 $this->load->database ();
	 $this->load->model('registroFormulario');
	 $this->registroFormulario-> permisos('estado_sitio ',$id);
	 $this->db->close ();
	 redirect('administrador/configuracion');
   }   
}
?>
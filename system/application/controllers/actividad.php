<?php
/**
* Contralador: Gestiona el inicio de session de los usaurios
* Fecha: 05 de marzo del 2010
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Actividad extends Controller {

	function Actividad()
	{
		parent::Controller();	
	}
/*******************************************************************************************
 Accion que presenta la interfas de usuario de acuerdo al perfil
*******************************************************************************************/
function index ()
{}




/*******************************************************************************************
 Metodo que sierra la seccion
*******************************************************************************************/
   function salir ()
    {
	  $this->session->sess_destroy();
	  redirect('welcome');	
    }
/*******************************************************************************************
 Accion  que inicia el asesion del usuario
*******************************************************************************************/
   function login ()
    {
	   extract($_POST);	
	   if( isset($usuario) && isset($clave))
	     {
	       $this->load->database ();
	       $this->load->model('consultas');
		   $usuario = $this->consultas->validaUsuario($usuario,md5($clave));
	       $this->db->close ();
		   if($usuario != false)
		    {
			   if($usuario->estado == 0)
			    {
				  $mensaje = "Su usuario esta deshabilitado por favor comun&iacute;quese con el administrador del sistema ";
		          $this->session->set_flashdata('mensaje', $mensaje); 
			      redirect('welcome');		
				}
			   else
			    {
				  $datos = array(
								  'nombre' => $usuario->nombre,
								  'grupo' => $usuario->id_grupo,
								  'id' => $usuario->id,
								  'estado' => 'registrado'
								 );	
				  $this->session->set_userdata($datos);
				  if($usuario->id_grupo == 1)
				      redirect('administrador');
				  else	  
				  	  redirect('actividad');			  
				 	
			    }  
		    }
		   else
		    {
			   $mensaje = "El nombre de usuario o la contrase&ntilde;a estan errados por favor intentelo de nuevo";
		       $this->session->set_flashdata('mensaje', $mensaje); 
			   redirect('welcome');	
			}
		 }
	   else
	     {
			$mensaje = "Huy parece que intentaste ingresar de una forma algo dudosa por favor intentalo de nuevo";
		    $this->session->set_flashdata('mensaje', $mensaje); 
			redirect('welcome');
		 }
	}
}

?>
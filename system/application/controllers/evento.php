<?php
/**
* Contralador: Gestiona la generacion de evectos del  programa
* Fecha: 13 de marzo del 2010
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Evento extends Controller {

	function Eveto ()
	{
		parent::Controller();	
	}
/******************************************************************************************
 Accion que lista todos los eventos de una entidad
******************************************************************************************/
function listaEvento($id=0)
 {
   if($id != 0)
    {
	  $this->load->database ();
	  $this->load->model('camposRegistro');
	  $this->load->model('eventos');
	  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	  $dato['entidad'] = $this->camposRegistro->informacionEntidad($id);
	  $dato['evento'] = $this->eventos->listaEntidad($id);
	  $this->db->close ();
	  $this->load->view('administrador/header',$info);
	  $this->load->view('evento/listaEvento',$dato);
	  $this->load->view('footer');  
    }
   else
		redirect('actividad/salir');  	
 }
/*******************************************************************************************
 Accion que genera eventos para una entidad
*******************************************************************************************/
function nuevoEvento($id=0)
 {
   if($id != 0)
    {
	  $this->load->database ();
	  $info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
	  $dato['entidad'] = $this->camposRegistro->informacionEntidad($id);
	  $this->db->close (); 
	  $this->load->view('administrador/header',$info);
	  $this->load->view('evento/nuevoEvento',$dato);
	  $this->load->view('footer');  
	}
    else
		redirect('actividad/salir');  		 
 }
/******************************************************************************************
 Accion que elimina un evento 
******************************************************************************************/
function eliminaEvento($id=0,$entidad)
 {
    if($id != 0)
	 {
	    $this->load->database ();
		$this->load->model('eventos');
		if($this->eventos->eliminarEvento($id))
		 $mensaje = 'El evento no se puede eliminar porque contiene responsables <br> 1. Primero elimine los responsables';
		else
		 $mensaje = "Se elimino el eventos exitosamente";
		$this->session->set_flashdata('mensaje', $mensaje);
	    $this->db->close ();
		redirect("evento/listaEvento/$entidad");  	
	 }
	 else
		redirect('actividad/salir');  
 }
}
?>
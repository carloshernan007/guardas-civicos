<?php 
/**
* Controlador        : Realiza el registro de los usuerios del sistema
* Fecha de creacion  : 24 de  febrero del 2009
* Apliacion          : Control de guardas civicos
*/
class Registro extends Controller {

	function Registro()
	{
		parent::Controller();	
	}
/******************************************************************************************
  Accion  que registra cualquier perfil de usuario valida solo para el administrador
******************************************************************************************/
	function  usuario()
	{
		$this->load->database ();
		$this->load->model('camposRegistro');
		$dato['departamento'] = $this->camposRegistro->departamentos ();
		$dato['perfil'] = $this->camposRegistro->perfil ();
		$dato['etnia'] = $this->camposRegistro->etnia();
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$this->db->close ();
		$this->load->view('administrador/header',$info);
		$this->load->view('registro/usuario',$dato);
		$this->load->view('footer');
	}
/******************************************************************************************
  Accion que genera una lista de ciudades de acuerdo al departamento para ajax
******************************************************************************************/
   function ciudad($departamento = 1)
    {
	  $this->load->database ();
	  $this->load->model('camposRegistro');
	  $dato['ciudad'] = $this->camposRegistro->ciudad($departamento);
	  $this->db->close ();
	  $this->load->view('registro/ciudad',$dato);  
	}   

/*******************************************************************************************
  Accion que registra el nuevo usuario desde el perfil de administrador
*******************************************************************************************/
  function nuevoUsuario ()
   {
	 extract($_POST);
	 if(isset($nombreUsuario)  && $nombreUsuario != '' )
	  {
		 $fecha =date("Y-m-d h:i:s ");
	     $config['upload_path'] = './fotos/';
         $config['allowed_types'] = 'gif|png|jpg';
         $config['max_size'] = '1000';
         $config['max_width'] = '300';
         $config['max_height'] = '300';
		 $this->load->database ();
		 $this->load->model('registroFormulario','nuevo');
		 $this->load->library('upload',$config);
		 $foto = $_FILES['foto']['name'];

		 if ($this->upload->do_upload('foto'))
		   {
			   $fotoInfomacion = $this->upload->data();
			   $this->nuevo->nuevoUsuario(
										   addslashes($nombreUsuario),
										   md5($claveUsuario),
										   $perfil,
										   $contrato,
										   addslashes($nombre),
										   addslashes($apellido),
										   $cedula,
										   $sexo,
										   $tipoSangre,
										   $telefono,
										   $celular,
										   addslashes($email),
										   $departamento,
										   $ciudad,
										   $barrio,
										   $vereda,
										   $estrato,
										   addslashes($direccion),
										   $fechaNacimiento,
										   $nivelAcademico,
										   addslashes($aptitudDeportiva),
										   addslashes($aptitudArtistica),
										   $etnia,
										   $fecha,
										   $estadoCivil,
										   addslashes($fotoInfomacion['file_name'])
	  
										  );
		   }
		 else
		   {
			  echo 'no subio la foto ';   
		   }
		 $this->db->close ();
	  }
	 else
	   redirect('registro/usuario');
	   
   }
/********************************************************************************************
  Accion que verifica si el nombre de un usuario esta disponible para ajax
********************************************************************************************/
  function disponibilidad ($nombre)
   {
	  $this->load->database ();
	  $this->load->model('camposRegistro');
	  $dato = $this->camposRegistro->nombreUsuario($nombre);
	  $this->db->close (); 
	  echo json_encode($dato);
	  
   }
/********************************************************************************************
 Registro de usuarios libre
********************************************************************************************/
 function registroUsuario ()
  {
	$this->load->database ();
	$this->load->model('camposRegistro');
	$dato['departamento'] = $this->camposRegistro->departamentos ();
	$dato['perfil'] = $this->camposRegistro->perfil ();
	$dato['etnia'] = $this->camposRegistro->etnia();
	$info = $this->camposRegistro->estadoConfiguracion ();
	$this->db->close ();
	extract($_POST);

	
	if(!isset($registro))
     $this->load->view('prueba',$dato);  
	else
	 {
	   if($disponible == 0)
	     {
			$mensaje = "El nombre de usuario <b>$nombreUsuario</b> no esta disponible, por favor intente con otro ";
		    $this->session->set_flashdata('mensaje', $mensaje);
	        redirect('registro/registroUsuario');  
		 }
	   else
	     {
		   $fecha =date("Y-m-d h:i:s ");
   	       $config['upload_path'] = './fotos/';
           $config['allowed_types'] = 'gif|png|jpg';
           $config['max_size'] = '1000';
           $config['max_width'] = '300';
           $config['max_height'] = '300';	
		   $this->load->library('upload',$config);
 		   $foto = $_FILES['foto']['name'];
		    if ($this->upload->do_upload('foto'))
			 {
	            $this->load->database ();
	            $this->load->model('registroFormulario','nuevo');
                $fotoInfomacion = $this->upload->data();
				 $this->nuevo->nuevoUsuario(
											 addslashes($nombreUsuario),
											 md5($claveUsuario),
											 $info->nivel_predeterminado,
											 $contrato,
											 addslashes($nombre),
											 addslashes($apellido),
											 $cedula,
											 $sexo,
											 $tipoSangre,
											 $telefono,
											 $celular,
											 addslashes($email),
											 $departamento,
											 $ciudad,
											 $barrio,
											 $vereda,
											 $estrato,
											 addslashes($direccion),
											 $fechaNacimiento,
											 $nivelAcademico,
											 addslashes($aptitudDeportiva),
											 addslashes($aptitudArtistica),
											 $etnia,
											 $fecha,
											 $estadoCivil,
											 addslashes($fotoInfomacion['file_name'])
		
											);
	            $this->db->close ();
				$mensaje = "El usuario $nombreUsuario se registro correctamente";
				$this->session->set_flashdata('mensaje', $mensaje);
				redirect('welcome');

			 }
			else
			 {
				$mensaje = "<img src='".base_url ()."images/nada.png'>Se presento un inconveniente al tratar de subir su fotografia por favor verifique <br>
				              <ul>
							    <li> El tama&ntilde;o no es superior a 900K</li>
								<li> Las dimenciones de la fotografia no son superiores a 300PX x 300PX
						      </ul>		
				            "; 
			    $this->session->set_flashdata('mensaje', $mensaje);
	            redirect('registro/registroUsuario');  				
			 }   
		 }
	 }
  }
/*******************************************************************************************
 Accion que genera la  lista de barrios por cada ciudad
*******************************************************************************************/
function listaBarrios($id = 0)
  {
	 $this->load->database ();
	 $this->load->model('camposRegistro');
	 $dato['barrio'] = $this->camposRegistro->barrio($id);
	 $this->db->close ();
	 $this->load->view('registro/listaBarrios',$dato);
  }
/*******************************************************************************************
 Accion que desactiva o activa un usuario
*******************************************************************************************/
function activarUsuario($estado,$id)
 {
     $this->load->database ();
	 $this->load->model('registroFormulario');
	 $this->registroFormulario->activar($estado,$id);
	 $this->db->close ();
	 redirect("usuario/informacionUsuario/$id"); 
 }
/*******************************************************************************************
 Accion que genera la lista de corregimientos
*******************************************************************************************/
function listaCorregimiento($id=0)
 {
	$this->load->database ();
	 $this->load->model('camposRegistro');
	 $dato['corregimiento'] = $this->camposRegistro->corregimiento ($id);
	 $this->db->close ();
	$this->load->view('registro/listaCorregimiento',$dato);
 }
/********************************************************************************************
 Accion que genera la lista de veredas
********************************************************************************************/
function listaVereda($id)
{
	$this->load->database ();
	$this->load->model('camposRegistro');
	$dato['vereda'] = $this->camposRegistro->veredas($id);
	$this->db->close ();
	$this->load->view('registro/listaVereda',$dato);
	
}
}
?>
<?php

class Welcome extends Controller {

	function Welcome()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$this->load->database ();
		$this->load->model('camposRegistro');
        $dato['estado'] = $this->camposRegistro->estadoConfiguracion ();
		$this->db->close ();		
		$this->load->view('home',$dato);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
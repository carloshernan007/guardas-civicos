<?php
/**
* Contralador: Gestiona todo sobre los responsables de un evento
* Fecha: 22 de marzo del 2010
* Proyecto: Aplicaciones para gestion los guardas civicos
*/
class Responsable extends Controller {

	function Responsable ()
	{
		parent::Controller();	
	}
/*****************************************************************************************
  Accion que lista los responsables de un evento
*****************************************************************************************/
    function evento ($id=0,$entidad=0)
	 {
	   if($id != 0)
	    {
		   $this->load->database ();
		   $this->load->model('camposRegistro');
		   $this->load->model('responsables');
		   $this->load->model('eventos');
		   $dato['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		   $dato['entidad'] = $this->camposRegistro->informacionEntidad($entidad);
		   $dato['responsable'] = $this->responsables->usuarioResposable($id);
		   $dato['evento'] = $this->eventos->informacion($id);
		   $this->load->view('administrador/header',$dato);
		   $this->load->view('responsable/evento',$dato);
		   $this->load->view('footer');	
		   $this->db->close ();
		}
	  else
	      redirect('actividad/salir');   
   }
 /****************************************************************************************
  Accion que asigna los responsables  aun evento
 ****************************************************************************************/
 function add ()
  {
     extract($_POST);	 
	 if(isset($entidad))
	  {
		$this->load->database ();
		$this->load->model('eventos');
		$this->load->model('consultas');
		$info['informacion'] = $this->camposRegistro->estadoConfiguracion ();
		$dato = array ( 'departamento'  => $this->camposRegistro->departamentos(),
						'ciudad'  =>  $this->camposRegistro->ciudad($departamento),
						'evento' => $this->eventos->informacion($evento),
						'perfil' => $this->camposRegistro->perfil (),
						'departamento_activo' => $departamento,
						'ciudad_activa' => $ciudad,
						'perfil_activo' => $perfil,
						'usuario'  => $this->consultas-> buscarUsuario($departamento,$ciudad,$perfil,0,0,0),
						'entidad' => $entidad,
					  );
		$this->db->close ();
		$this->load->view('administrador/header',$info);
		$this->load->view('responsable/add',$dato);
		$this->load->view('footer');	
						
	  }
	 else 
	   redirect('actividad/salir');   
  }
 /*****************************************************************************************
  Accion que asigna el personal a un evento 
 *****************************************************************************************/
 function asigna ()
  {
	 extract($_POST);
	 if(isset($evento))
	  {
		$this->load->database ();
		$this->load->model('responsables');
		$error = 0;
		$fecha =date("Y-m-d h:i:s ");
		foreach($asigna as $usuario)
		  $error+= $this->responsables->asignaResponsable($evento,$usuario,$fecha); 	  
		$this->db->close  ();
		if($error > 0)
		 $mensaje = 'El sistema no asigno algunos usuarios porque estos ya pertenecen al evento!';
		else
		 $mensaje = "Se asignaron los usuarios al evento";
		 $this->session->set_flashdata('mensaje', $mensaje);
		 redirect("responsable/evento/$evento/$entidad");
	  }
	 else
	  redirect('actividad/salir');  
  }
}
?>
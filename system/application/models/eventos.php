<?php
class Eventos extends Model {
  
  function Eventos ()
  {
    parent::Model(); 
  }
/*****************************************************************************************
 Metodo que lista  todos los eventos de una entidad
*****************************************************************************************/
function listaEntidad($id)
 {
   $sql ="select E.id_evento,E.nombre,E.fecha_creacion,E.fecha_inicio,E.fecha_terminacion,T.nombre as tipo, U.nombre as usuario ";
   $sql .="from evento E inner join tipo_evento T on E.id_tipo = T.id_tipo ";
   $sql .="inner join usuario U on E.id_usuario = U.id ";
   $sql .="where  E.id_entidad = $id ";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->result (); 
	else
	return false ;
 }
/*****************************************************************************************
 Metodo que elimina un eventos siempre y cuando este no contenga responsables
*****************************************************************************************/
function eliminarEvento($id)
 {
    $sql="select id_responsable from responsable where id_evento = $id"; 
	$consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->result (); 
	$this->db->delete('evento', array('id_evento' => $id));	
	return true;
 }
/*****************************************************************************************
 Metodo que  genera la informacion de un evento
*****************************************************************************************/
function informacion ($id)
 {
   $sql ="select E.id_evento,E.nombre,E.fecha_creacion,E.fecha_inicio,E.fecha_terminacion,T.nombre as tipo, U.nombre as usuario, U.id  as id,  ";
   $sql .="E.id_ciudad, C.nombre as ciudad , C.id_departamento ";
   $sql .="from evento E inner join tipo_evento T on E.id_tipo = T.id_tipo ";
   $sql .="inner join usuario U on E.id_usuario = U.id ";
   $sql .="inner join ciudad C on E.id_ciudad = C.id_ciudad ";
   $sql .="where  E.id_evento = $id ";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->row (); 
	else
	return false ; 
 }
}
?>
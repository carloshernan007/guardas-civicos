<?php
/**
* Modelo: Administra toda la informacion sobre los responsables de un eventos
* Fecha: 24 de marzo del 2010
* Asunto: Proyecto de guardas Civicos
*/
class Responsables extends Model {
  
  function Responsables ()
  {
    parent::Model(); 
  }
/*******************************************************************************************************
  Metodo que genera la lista de responsables de  un evento
*******************************************************************************************************/
  function usuarioResposable ($id)
   {
     $sql = "select nombreUsuario,grupo,estado,nombre,apellido,cedula,email,departamento,ciudad,contrato,celular,
	                foto,id,R.fecha as fecha ";
    $sql .="from vista_informacion_usuario V, responsable R";					  
	$sql .=" where R.id_evento = $id and V.id = R.id_usuario";
	$consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->result (); 
	else
	return false ;
   }
/*******************************************************************************************
 Metodo que registra los responsables de un  evento
*******************************************************************************************/
function asignaResponsable($evento,$usuario,$fecha)
{
   $sql="select id_responsable from responsable where id_usuario = $usuario and  id_evento = $evento";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return 1;	 
	$this->db->insert('responsable', array('id_evento' => $evento, 'id_usuario' => $usuario, 'fecha' => $fecha));	  
	return 0;
} 
/********************************************************************************************************
 Acccion que elimina un responsable
********************************************************************************************************/
function elimina($usuario,$evento)
 {
	 
 }
}

?>
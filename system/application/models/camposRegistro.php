<?php
/**
* Modelo: Genera la informacion sobre todos los campos complementarios de los formulario de registro
*/
class CamposRegistro extends Model {
  
  function CamposRegistro ()
  {
    parent::Model(); 
  }
/******************************************************************************************
  Metodo que  genera la lista de todos los departamentos
******************************************************************************************/
function departamentos ()
 {
    $sql = "select id_departamento, nombre from departamento order by nombre asc";
	$consulta = $this->db->query($sql);
	return $consulta->result (); 
 }
/******************************************************************************************
  Metodo que genera la lista de ciudades por depaartamentos
******************************************************************************************/
 function ciudad ($departamento)
 {
    $sql="select id_ciudad,nombre from ciudad where id_departamento = $departamento order by nombre asc";
    $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->result (); 
	else
	return false ;
 }
/******************************************************************************************
  Metodo que genera la lista de los perfiles de usuario
*******************************************************************************************/
function perfil ()
{
  $sql="select id_grupo,nombre from grupos  order by nombre asc";
  $consulta = $this->db->query($sql);
  return $consulta->result (); 
}
/******************************************************************************************
  Metodo que genera la lista de las etnias
******************************************************************************************/
 function etnia()
 {
    $sql="select id_etnia,nombre from etnia order by nombre asc";
    $consulta = $this->db->query($sql);
	return $consulta->result (); 
 }
/******************************************************************************************
  Metodo que valida si un usuario exite 
******************************************************************************************/
function nombreUsuario($nombre)
 {
   $sql="select id from usuario where  nombre = '$nombre'";
   $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return 0;
   else
     return 1;
 }
/*****************************************************************************************
  Metodo que genera la lista de barrios por cada ciudad
*****************************************************************************************/
 function barrio($id)
 {
   $sql="select id_barrio,nombre,id_comuna from barrio where id_ciudad = $id and id_barrio <> 1 order by nombre asc  ";	 
   $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->result ();
   else
     return false;	 
 }
 /*****************************************************************************************
  Metodo que genera la lista de barrios por cada ciudad
*****************************************************************************************/
 function barrioFiltro($id,$comuna)
 {
   $sql="select id_barrio,nombre,id_comuna from barrio where id_ciudad = $id and id_barrio <> 1 and ";
   $sql .=" id_comuna = $comuna order by nombre asc  ";	 
   $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->result ();
   else
     return false;	 
 }
 /*****************************************************************************************
  Metodo que genera la lista de barrios por cada ciudad
*****************************************************************************************/
 function corregimiento ($id)
 {
   $sql="select id_ciudad,nombre,id_corregimiento from corregimiento where id_ciudad = $id  order by nombre asc  ";	 
   $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->result ();
   else
     return false;	 
 }
/*****************************************************************************************
 Metodo que genera la configuracion  globla del portal
*****************************************************************************************/
 function estadoConfiguracion ()
  {
	$sql="select id,nombre_sitio,registro_usuarios,nivel_predeterminado,estado_sitio,email from configuracion where id = 1  "; 
	$consulta = $this->db->query($sql);
	return $consulta->row(); 
  }
/*****************************************************************************************
 Metodo que retorna el nombre de un departamento
*****************************************************************************************/
 function nombreDepartamento($id)
  {
	 $sql = "select id_departamento, nombre from departamento where id_departamento = $id";
	$consulta = $this->db->query($sql);
	return $consulta->row ();  
  }
/*******************************************************************************************
Metodo que genera toda la informacion sobre una ciudad
*******************************************************************************************/
function informacionCiudad($id)
 {
    $sql="select id_ciudad,id_departamento, nombre from ciudad where id_ciudad = $id";	
	$consulta = $this->db->query($sql);
	return $consulta->row ();
 }
/*******************************************************************************************
Metodo que genera toda la informacion sobre un Barrio
*******************************************************************************************/
function informacionBarrio($id)
 {
    $sql="select id_barrio,id_comuna,id_ciudad,nombre from barrio where id_barrio = $id";	
	$consulta = $this->db->query($sql);
	return $consulta->row ();
 }  
/*******************************************************************************************
Metodo que genera toda la informacion sobre un Corregimiento
*******************************************************************************************/
function informacionCorregimiento($id)
 {
    $sql="select id_corregimiento,nombre,id_ciudad from corregimiento where id_corregimiento = $id";	
	$consulta = $this->db->query($sql);
	return $consulta->row ();
 }   
/*******************************************************************************************
 Metodo que genera la lista de las veredas de un corregimiento
*******************************************************************************************/
function veredas($corregimiento)
{
  $sql="select id_vereda,nombre,id_corregimiento from vereda  where id_corregimiento = $corregimiento order by nombre asc";	
  $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->result ();
   else
     return false;	
}
/*******************************************************************************************
 Metodo que genera la informacion sobre una vereda
*******************************************************************************************/
function informacionVereda ($id)
{
	$sql="select id_vereda,nombre,id_corregimiento from vereda  where id_vereda = $id";	
  $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->row ();
   else
     return false;	
}
/*******************************************************************************************
 Metodo que obtiene la lista de entidades
*******************************************************************************************/
function entidades ($departamento,$ciudad,$sector)
{
  $sql ="select E.nombre,E.id_entidad,E.fecha,sector,D.nombre as departamento,C.nombre as ciudad ";
  $sql .="from entidad_solicitante E inner join  ciudad C  on E.id_ciudad = C.id_ciudad ";
  $sql .="inner join departamento D on D.id_departamento = C.id_departamento ";
  $cont = 0;
  if($departamento != 0){
   $sql .="where C.id_departamento = $departamento ";
   $cont++;
  }
  if($ciudad != 0){
   $sql .="and C.id_ciudad = $ciudad";
   $cont++;
  }
  if($cont > 0 && $sector != -1){
   $sql .="and E.sector = $sector ";
  }
  if($cont == 0 && $sector != -1){
   $sql .="where E.sector = $sector ";
  }
  
  $sql ."order by E.nombre asc ";
  
  $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->result ();
   else
     return false;	
  
}
/******************************************************************************************
Metodo que obtiene informacion sobre una entidad 
******************************************************************************************/
function informacionEntidad($id)
{
  $sql ="select E.nombre,E.id_entidad,E.fecha,sector,D.nombre as departamento,C.nombre as ciudad ";
  $sql .="from entidad_solicitante E inner join  ciudad C  on E.id_ciudad = C.id_ciudad ";
  $sql .="inner join departamento D on D.id_departamento = C.id_departamento ";
  $sql .=" where E.id_entidad = $id";
   $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->row ();
   else
     return false;	
}
/*****************************************************************************************
Metodo que genera los niveles de escolaridad
*****************************************************************************************/
function escolaridad ()
{
  $sql="select id,nombre from escolaridad order by nombre asc";	
  $consulta = $this->db->query($sql);
   if($consulta->num_rows () > 0 )
     return $consulta->result ();
   else
     return false;	
}
}
?>
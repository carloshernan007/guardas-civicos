<?php
/**
* Modelo: Que registra los datos de los usuarios
*/
class RegistroFormulario extends Model {
  
  function RegistroFormulario ()
  {
    parent::Model(); 
  }
/******************************************************************************************
 Metodo  que registra un nuevo usuario
******************************************************************************************/
  function nuevoUsuario ($nombreUsuario,$claveUsuario,$perfil, $contrato,$nombre,$apellido,$cedula,$sexo,
									 $tipoSangre,$telefono,$celular,$email,$departamento,$ciudad,
									 $barrio,$vereda,$estrato,$direccion,$fechaNacimiento,$nivelAcademico,
          							 $aptitudDeportiva,$aptitudArtistica,$etnia,$fecha,$estadoCivil,$foto){
	  
	$sql = "insert into usuario (nombre,clave,fecha_creacion,id_grupo) ";
	$sql .= "values ('$nombreUsuario','$claveUsuario','$fecha','$perfil')";
	$this->db->query($sql);
	$id_usuario = $this->db->insert_id(); 
	$sql ="insert into persona (id_usuario,contrato,nombre,apellido,cedula,sexo,tipo_sangre,fecha_nacimiento,estado_civil,direccion,
							   telefono,celular,email,id_departamento,id_ciudad,id_barrio,id_vereda,estrato,nivel_academico,
							   aptitud_deportiva,aptitud_artistica,etnia,foto)  ";
	
	$sql .="values ($id_usuario,$contrato,'$nombre','$apellido',$cedula,'$sexo','$tipoSangre','$fechaNacimiento',
					'$estadoCivil','$direccion',$telefono,$celular,'$email',$departamento,
					$ciudad,$barrio,$vereda,$estrato,$nivelAcademico,'$aptitudDeportiva','$aptitudArtistica',
					$etnia,'$foto')";
	
	$this->db->query($sql);   
   }
/*******************************************************************************************
Metodo que cambia los estados del sitio 
estado_sitio : Permite inicar el mantenimiento del sitio
registro_usuarios : permite el registor de usuarios desde el home
*******************************************************************************************/
function permisos($atributo,$campo)
{
  $dato["$atributo"] = $campo;
  $this->db->where('id', 1);
  $this->db->update('configuracion', $dato);
}
/******************************************************************************************
 Metodo que actualiza la configuracon global 
******************************************************************************************/
function configuracionGlobal($nombre,$nivelAcceso,$email)
{
	$dato = array('nombre_sitio' => $nombre,'nivel_predeterminado' => $nivelAcceso,'email' => $email);
	$this->db->where('id', 1);
    $this->db->update('configuracion', $dato);
}
/*******************************************************************************************
 Metodo que desactiva o activa un usuario
*******************************************************************************************/
function activar($estado,$id)
{
  $dato['estado'] = $estado;	
  $this->db->where('id', $id);
  $this->db->update('usuario', $dato);
  
}
/******************************************************************************************
 Metodo que actualiza el nombre de un departamento
******************************************************************************************/
function actualizaDepartamento($id,$nombre)
{
   $sql="select id_departamento nombre from departamento where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
	else
	 {
		$this->db->where('id_departamento', $id);
        $this->db->update('departamento', array('nombre' => $nombre));
		return  true;
     }
}
/*******************************************************************************************
 Metodo que elimina un departamento siempre que este no tenga ciudades asignadas
*******************************************************************************************/
function eliminaDepartamento($id)
 {
   $sql="select id_departamento from ciudad where id_departamento = $id";	
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
    $this->db->delete('departamento', array('id_departamento' => $id));	
	return true;
 }
/*******************************************************************************************
 Metodo que  registra un nuevo departamento
*******************************************************************************************/
function nuevoDepartamento($nombre)
 {
   $sql="select id_departamento nombre from departamento where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
    $this->db->insert('departamento', array('nombre' => $nombre));	 
    return true;	
 }
/*******************************************************************************************
 Metodo que registra  una nueva ciudad
*******************************************************************************************/
function nuevaCiudad($nombre, $id)
{
	$sql="select id_ciudad nombre from ciudad where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->insert('ciudad', array('id_departamento' => $id, 'nombre' => $nombre));	  
	return true;
}
/*******************************************************************************************
metodo que actualiza  la ciudad
*******************************************************************************************/
function actualizaCiudad($nombre,$departamento,$id)
 {
    $this->db->where('id_ciudad', $id);
    $this->db->update('ciudad', array('nombre' => $nombre, 'id_departamento' => $departamento));
	return true;
 }
/*******************************************************************************************
Metodo que elimina una ciudad siempre que esta no contenga  barrios o veredas
*******************************************************************************************/
function eliminaCiudad($id)
{
	$sql="select id_ciudad from barrio where id_ciudad = $id";	
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
    $this->db->delete('ciudad', array('id_ciudad' => $id));	
	return true;
}
/*******************************************************************************************
Metodo que  Actualiza un barrio
*******************************************************************************************/
function actualizaBarrio($nombre,$comuna,$ciudad,$id)
{   
    $sql="select id_barrio from barrio where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->where('id_barrio', $id);
    $this->db->update('barrio', array('nombre' => $nombre, 'id_comuna' => $comuna, 'id_ciudad' => $ciudad));
	return true;
}
/*******************************************************************************************
 Metodo que registra  un nuevo barrio
*******************************************************************************************/
function nuevaBarrio($nombre,$comuna,$ciudad)
{
	$sql="select id_barrio nombre from barrio where nombre = '$nombre' and id_comuna = $comuna";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->insert('barrio', array('id_comuna' => $comuna, 'nombre' => $nombre, 'id_ciudad' => $ciudad ));	  
	return true;
}
/*******************************************************************************************
 Metodo que elimina el barrio
*******************************************************************************************/
function eliminaBarrio ($id)
{
	 $sql="select id_barrio from persona where id_barrio = $id";	
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
    $this->db->delete('barrio', array('id_barrio' => $id));	
	return true;
}
/*******************************************************************************************
 Metodo que registra  un nuevo Corregimiento
*******************************************************************************************/
function nuevoCorregimiento($nombre,$ciudad)
{
	$sql="select id_corregimiento nombre from corregimiento where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->insert('corregimiento', array('id_ciudad' => $ciudad, 'nombre' => $nombre));	  
	return true;
}
/*******************************************************************************************
 Metodo que elimina un corregimiento
*******************************************************************************************/
function eliminaCorregimiento ($id)
{
	 $sql="select id_corregimiento from vereda where id_corregimiento = $id";	
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
    $this->db->delete('corregimiento', array('id_corregimiento' => $id));	
	return true;
}
/******************************************************************************************
 Metodo que actualiza la informacion de un corregimiento
******************************************************************************************/
function actualizaCorregimiento($ciudad,$nombre,$id)
{
   $sql="select id_corregimiento from corregimiento where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->where('id_corregimiento', $id);
    $this->db->update('corregimiento', array('nombre' => $nombre, 'id_ciudad' => $ciudad, 'nombre' => $nombre));
	return true;	
}
/******************************************************************************************
 metodo que registra una nueva vereda
******************************************************************************************/
function registraVereda($corregimiento,$nombre)
 {
	$sql="select id_vereda  from vereda where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->insert('vereda', array('id_corregimiento' => $corregimiento, 'nombre' => $nombre));	  
	return true; 
 }
/****************************************************************************************
Metodo que actualiza una vereda
*****************************************************************************************/
function actualizaVereda($corregimiento,$nombre,$id)
{
   $sql="select id_vereda from vereda where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->where('id_vereda', $id);
    $this->db->update('vereda', array('nombre' => $nombre, 'id_corregimiento' => $corregimiento, 'id_vereda' => $id));
	return true;	
}
/***************************************************************************************
Metodo que elimina una vereda siempre y cuando no tenga usuarios con refencia a estas
**************************************************************************************/
function eliminaVereda($id)
{
   $sql="select id_vereda from persona where id_vereda = $id";	
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
    $this->db->delete('vereda', array('id_vereda' => $id));	
	 return true;
}
/***************************************************************************************
Metodo que registra entidades
***************************************************************************************/
function nuevaEntidad($ciudad,$sector,$nombre,$fecha)
 {
	$sql="select id_entidad  from entidad_solicitante where nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;	 
	$this->db->insert('entidad_solicitante', array('id_ciudad' => $ciudad, 'nombre' => $nombre,'fecha' => $fecha,'sector' => $sector));	  
	return true;  
 }
/****************************************************************************************
Metodo que actualiza una vereda
*****************************************************************************************/
function actualizaEntidad($nombre,$ciudad,$sector,$departamento,$id)
{
	$this->db->where('id_entidad', $id);
    $this->db->update('entidad_solicitante', 
					  array('nombre' => $nombre,  'id_ciudad' => $ciudad,'sector' => $sector));
} 
/***************************************************************************************
Metodo que elimina una vereda siempre y cuando no tenga usuarios con refencia a estas
**************************************************************************************/
function eliminaEntidad($id)
{
   $sql="select id_entidad from evento where id_entidad = $id";	
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	  return false;
    $this->db->delete('entidad_solicitante', array('id_entidad' => $id));	
	 return true;
}
/**************************************************************************************
Metodo que permite registrar un nuevo  familiar
**************************************************************************************/
function addFamiliar ($usuario,$registro,$nombre,$apellido,$fecha_nacimiento,$parentesco,$deportiva,$artistica,$escolaridad,$fecha,$telefono)
{
   $dato = array('id_usuario' => $usuario,
				 'registro' => $registro,
				 'nombre' => $nombre,
				 'apellido' => $apellido,
				 'fecha_nacimiento' => $fecha_nacimiento,
				 'parentesco' => $parentesco,
				 'aptitud_deportiva' => $deportiva,
				 'aptitud_artistica' => $artistica,
				 'id_escolaridad' => $escolaridad,
				 'fecha_creacion' => $fecha,
				 'telefono' => $telefono);	
   $this->db->insert('familiar',$dato);	  	
}

}
?>
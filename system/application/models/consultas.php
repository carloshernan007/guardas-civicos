<?php
/**
* Modelo: Genera que contiene los modelos sobre las busquedas y reportes de los usuarios del sistema
* Fecha: 02 de marzo del 2010
* Asunto: Proyecto de guardas Civicos
*/
class Consultas extends Model {
  
  function Consultas ()
  {
    parent::Model(); 
  }
/*****************************************************************************************
 Metodo que permite realizar las consultas sobre los usaurios registrados en  el sistema
******************************************************************************************/
  function buscarUsuario($departamento,$ciudad,$perfil,$nombreUsuario,$apellido,$contrato)
  {
	$cont = 0;    
    $sql ="select id,nombreUsuario,estado,grupo as perfil,nombre,apellido,ciudad,contrato,departamento,ciudad,grupo, ";
	$sql .="cedula ";
    $sql .=" from vista_informacion_usuario ";
	if( $departamento != 0 || $ciudad != 0 || $perfil != 0 || $nombreUsuario != '' || $apellido != '' ||  $contrato != '')
	$sql .="where ";
	if( $departamento != 0){
	$sql .="id_departamento = $departamento ";
	$cont ++;
	}
	if($ciudad != 0 && $cont > 0){
	   $sql .="and id_ciudad = $ciudad ";
	   $cont++;
	 }
	 else{ 
	       if($ciudad != 0){
		   $sql .="id_ciudad = $ciudad  "; 
		   $cont++;}
		 }
	 
	if( $perfil != 0 && $cont > 0){
	$sql .="and id_grupo = $perfil ";
	$cont++;
	}else{
	       if($perfil != 0){
	         $sql .=" id_grupo = $perfil ";
		     $cont++;}
	     }
	
	if( $nombreUsuario != '' && $cont > 0){
	   $sql .="and nombre = '$nombreUsuario' ";
	   $cont++;
	}else{
		   if($nombreUsuario != ''){
		     $sql .=" nombre = '$nombreUsuario' ";
		     $cont++;}
	     }
	
	if( $apellido != '' && $cont > 0){
	$sql .="and apellido  like '*$apellido*' ";
	$cont ++;
	}else{
		  if($apellido != ''){
	        $sql .=" apellido like '*$apellido*' ";
		    $cont++;}
	     }
	
	if( $contrato != '' && $cont > 0){
	$sql .=" and contrato = $contrato ";}
	else{
		 if($contrato != ''){
		  $sql .="  contrato = $contrato ";
		  $cont++;}
	    }
	
	$consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->result (); 
	else
	return false ;
  }
/*******************************************************************************************************
  Metodo que genera la informacion del personal del usuario
*******************************************************************************************************/
 function informacionPersonal ($id)
  {
     $sql = "select nombreUsuario,fecha_creacion,grupo, estado,nombre,apellido,cedula,
					  sexo,email,departamento,ciudad,contrato,nivel_academico,etnia,
					  celular,estrato,foto,barrio,vereda,estado,fecha_nacimiento,estado_civil,direccion,telefono,estado,id ";
    $sql .="from vista_informacion_usuario";					  
	$sql .=" where  id = $id";				
	$consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->row (); 
	else
	return false ;
  }
/********************************************************************************************************
 Metodo que lista la  informacion de los parientes del usuario
********************************************************************************************************/
function informacionPariente($id)
{
   $sql = "select id_familiar,F.nombre,apellido,fecha_nacimiento,parentesco,aptitud_deportiva,aptitud_artistica,E.nombre as escolaridad, telefono";
   $sql .="  from familiar F, escolaridad E ";
   $sql .="where F.id_usuario = $id and E.id = F.id_escolaridad order by F.nombre asc";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->result (); 
	else
	return false ;
}
/*******************************************************************************************************
 Metodo que valida si un usuario existe
*******************************************************************************************************/
function validaUsuario($nombre,$clave)
 {
   $sql ="select nombre,id, estado,id_grupo from  usuario ";
   $sql .="where clave = '$clave' and  nombre = '$nombre'";
   $consulta = $this->db->query($sql);
	if($consulta->num_rows () > 0 )
	return $consulta->row (); 
	else
	return false ;
 }
}
?>
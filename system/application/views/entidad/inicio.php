<h1><?=$titulo?></h1>

<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('entidad/',$atrivutos ); ?>
<table>
 <thead>
  <th width="1px">#</th>
  <th width="20%">Nombre</th>
  <th width="2%">Sector <br>
       <select id="sector" name="sector" class="filtro">
        <option value="-1">Selecione</option>
        <option value="-1">Todos</option>
        <option value="0">Privados</option>
        <option value="1">Publicos</option>
       </select>
 </th>
  <th>Deparamento<br>
      <select name="departamento" id="departamento" class="filtro">
       <option value="0">Seleccione</option>
       <option value="0">Todas</option>
      <?php foreach($departamento as $item): ?>
       <?php if($item->id_departamento == $d): ?>
       <option value="<?=$item->id_departamento?>" selected="selected"><?=$item->nombre?></option>
       <?php else: ?>
       <option value="<?=$item->id_departamento?>" ><?=$item->nombre?></option>
       <?php endif; ?>
      <?php endforeach;?> 
      </select>
  </th>
  <th>Ciudad<br>
      <select name="ciudades" id="ciudad" class="filtro">
       <option value="0">Seleccione</option>
       <option value="0">Todas</option>
       <?php if($d != 0 ): ?>
         <?php foreach($ciudad as $item): ?>
           <option value="<?=$item->id_ciudad?>" ><?=$item->nombre?></option>
         <?php endforeach;?>
       <?php endif;?>
      </select>
  </th>
  <th>Fecha de registro</th>
  <th width="22%">Acciones</th>
 </thead>
<tr>
  <td colspan="7"  class="action">
 <?php $campo['class'] = 'view'; echo anchor("entidad/add/", 'Nueva', $campo) ; ?> </td>
</tr> 

<?php if($entidades == false): ?>
<tr><td>El sistema no pudo encontrar registros</td></tr>
<?php else: ?>
 <?php $cont=1; foreach($entidades as $item): ?>
  <tr>
    <td><?=$cont?></td>
    <td><?=$item->nombre?></td>
    <td><?php if($item->sector == 1):?> Publico <?php else: ?> Privado <?php endif;?></td>
    <td><?=$item->departamento?></td>
    <td><?=$item->ciudad?></td>
    <td><?=$item->fecha?></td>
    <td class="action">
  <?php $campo['class'] = 'view'; echo anchor("evento/listaEvento/$item->id_entidad", 'Eventos', $campo) ; ?>&nbsp;
  <?php $campo['class'] = 'edit'; echo anchor("entidad/editar/$item->id_entidad", 'Edit', $campo) ; ?>
  <?php $campo['class'] = 'delete';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("entidad/eliminar/$item->id_entidad", 'eliminar', $campo) ; ?>
  </td>
    
  </tr>  
 <?php $cont++; endforeach; ?>

<?php endif; ?>


</table>

</form>
<h1>Configuración Global </h1>
<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

<?php echo form_open('administrador/actualizarSitio'); ?>
<fieldset>
<legend> Configuración global del sitio </legend>

<div class="formulario extra">
 <label class="extra">Nombre del sitio web</label>
 <input name="nombre" value="<?=$estado->nombre_sitio ?>" />
</div>

<div class="formulario extra">
 <label class="extra">Permitir el registro de usuarios desde el home ?</label>
 <?php if($estado->registro_usuarios == 1 ) : ?>
  <a href="<?php echo base_url ();?>index.php/administrador/registro/0">
 <img src="<?php echo base_url ();?>images/ok.png" >
 <?php else: ?>
  <a href="<?php echo base_url ();?>index.php/administrador/registro/1">
 <img src="<?php echo base_url ();?>images/cancel.png" >
 <?php endif;?>
 </a>
</div>

<div class="formulario extra">
 <label class="extra">Sitio desactivado?</label>

 <?php if($estado->estado_sitio  == 1 ) : ?>
  <a href="<?php echo base_url ();?>index.php/administrador/sitio/0">
 <img src="<?php echo base_url ();?>images/ok.png" >
 <?php else: ?>
  <a href="<?php echo base_url ();?>index.php/administrador/sitio/1">
 <img src="<?php echo base_url ();?>images/cancel.png" >
 <?php endif;?>
 </a>
</div>

<div class="formulario extra">
 <label class="extra">Nivel de acceso predeterminado</label>
 <select name="nivelAcceso" >
 <?php foreach($perfil as $item): ?>
  <?php if($estado->nivel_predeterminado  == $item->id_grupo ): ?>
    <option value="<?=$item->id_grupo?>" selected="selected="><?=$item->nombre?></option>
  <?php else: ?>
    <option value="<?=$item->id_grupo?>"><?=$item->nombre?></option>
  <?php endif; ?>
 <?php endforeach; ?>
 </select>
</div>

<div class="formulario extra">
 <label class="extra">E-mail de seguimiento</label>
 <input name="email" value="<?=$estado->email ?>" />
</div>
<input type="submit" value="Actualizar" class="button" />
</form>
</fieldset>
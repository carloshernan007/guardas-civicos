<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$informacion->nombre_sitio?></title>

<!-- CSS -->
<link href="<?php  echo base_url (); ?>css/transdmin.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url (); ?>css/menu.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url (); ?>js/fancybox/jquery.fancybox-1.3.1.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url (); ?>css/ui.datepicker.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url (); ?>css/ie6.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url (); ?>css/ie7.css" /><![endif]-->

<!-- JavaScripts-->
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/jNice.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/hoverIntent.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/superfish.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/funciones.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/ui.core.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.validate.js"></script>

<script type="text/javascript" src="<?php echo base_url (); ?>js/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/fancybox/jquery.fancybox-1.3.1.js"></script>
</head>

<body>
	<div id="wrapper">
    	<!-- h1 tag stays for the logo, you can use the a tag for linking the index page -->
    	<h1><a href="#"><span>Transdmin Light</span></a></h1>
    
    <div id="zona-menu">    
       <ul class="sf-menu" id="nav"> 
         <li><a href="#">Sitio</a>
           <ul>
             <li><?php echo anchor('administrador/', 'Control panel'); ?></li>
             <li><?php echo anchor('administrador/configuracion', 'Configuraci&oacute;n'); ?></li>
           </ul>
           </li>   
       	      <li><a href="#">Gestor de Usuarios</a>  
		    <ul>
  			  <li><?php echo anchor('registro/usuario', 'Nuevo Usuario'); ?></li>
              <li><?php echo anchor('usuario', 'Busca Usuario'); ?></li>
			</ul>
		</li>
            <li><a href="#">Gestor de localizacion</a> 
             <ul>
               <li><?php echo anchor('localizacion', 'Departamentos');?> </li>
               <li><a href="#">Ciudades</a></li>
               <li><a href="#">Barrios</a> </li>
               <li><a href="#">Veredas</a> </li>
             </ul>
            </li>
			<li><a href="#">Gestor de Eventos</a>
              <ul>
                <li><?php echo anchor('entidad', 'Entidades');?></li>
                <li><?php echo anchor('localizacion', 'Eventos');?></li>
              </ul>
            </li>
            <li><a href="#">Informes</a> </li>
			<li><?php echo anchor('actividad/salir', 'Salir');?> </li>			
		</ul>
      <div class="clear"> </div>  
    </div>    
        <!-- // #end mainNav -->
        
        <div id="containerHolder">
			<div id="container">
              <? /*
        		<div id="sidebar">
                	<ul class="sideNav">
                    	<li><a href="#">Informes</a></li>
                    	<li><a href="#" class="active">Print resources</a></li>
                    	<li><a href="#">Training &amp; Support</a></li>
                    	<li><a href="#">Books</a></li>
                    	<li><a href="#">Safari books online</a></li>
                    	<li><a href="#">Events</a></li>
                    </ul>
                    <!-- // .sideNav -->
                </div>
			*/ ?>	
                <!-- // #sidebar -->
                
                <!-- h2 stays for breadcrumbs -->
                <h2><a href="#">Dashboard</a> &raquo;<a href="#" class="active">Print resources</a></h2>
                
                <!-- Inicia el contenido -->
                <div id="main">
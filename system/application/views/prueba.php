<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php  echo base_url (); ?>css/transdmin.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/funciones.js"></script>
<link href="<?php echo base_url (); ?>css/ui.datepicker.css" rel="stylesheet" type="text/css" media="screen" />
</head>

<body>
<div id="contenedor-registro">
 <div id="cabeza"><h1> Formulario de registro </h1></div>
 <div id="contenido">
 <div id="bloque-izquierdo"></div>
 <div id="bloque-derecho">
 <div id="formulario">
 <?php 
$parametros =  array('id' => 'formulario', 'onsubmit' => 'return validacion()');
echo form_open_multipart('registro/registroUsuario',$parametros);?>

<fieldset>
 <legend> Información del usuarios </legend>
<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>


                            
 <div class="formulario">
   <label>Nombre de usuario:</label>
   <input type="text"  name="nombreUsuario"  title="Por favor ingrese su nombre de usuario" class="requerido"/>
 </div>
 
 <div class="formulario">
  <label>Contraseña</label>
  <input type="password"  name="claveUsuario" id="claveUsuario" title="Por favor ingrese su clave de usuario"  class="requerido"/>
 </div>
 
  <div class="formulario">
  <label>Confirme contraseña</label>
  <input type="password" name="confirmaClave" title="Por favor confirme su clave " class="requerido" />
 </div>
</fieldset>

<fieldset>
 <legend> Informacion personal </legend>
 <div class="formulario">
   <label>Numero de contrato:</label>
   <input type="text" name="contrato" title="Por favor indique su numero de contrato solo numeros" class="requerido"/>
 </div>
 
  <div class="formulario">
   <label>Nombres:</label>
   <input type="text" name="nombre" title="Por favor ingresa tu nombre"  class="requerido" />
  </div>
  
  <div class="formulario">
   <label>Apellidos:</label>
   <input type="text" name="apellido" title="Por favor ingresa tu apellido" class="requerido"/>
  </div>
  
  <div class="formulario">
   <label>Cedula:</label>
   <input type="text" name="cedula" title="Por favor ingresa tu numero de cedula solo numeros"  class="requerido"/>
  </div>
  
  <div class="formulario">
   <label>Grupos Sanguineo:</label>
   <select name="tipoSangre">
    <option value="O+"> O+ </option>
    <option value="O-"> O- </option>
    <option value="A+"> A+ </option>
    <option value="A-"> A- </option>
    <option value="B+"> B+ </option>
    <option value="B-"> B- </option>
    <option value="AB+"> AB+ </option>
    <option value="AB-"> AB- </option>
    
   </select>
  </div>
  <div class="formulario">
   <label>Estado civil</label>
   <select name="estadoCivil" title="Por favor selecciona tu estado civil"  class="requerido" />
   <option value="0">Selecciones</option>
   <option value="soltero">Soltero</option>
   <option value="casado">Casado</option>
   <option value="union">Union libre</option>
   <option value="separado">Separado</option>
   <option value="viudo">viudo</option>
   </select>
    
  </div>
  
  
  <div class="formulario">
   <label>Telefono:</label>
   <input type="text" name="telefono" title="Por favor ingresa tu numero de valido"  class="requerido" />
  </div>
  
  <div class="formulario">
   <label>Celular:</label>
   <input type="text" name="celular" title="Por favor ingresa tu numero de celular solo numeros" class="requerido" />
  </div>
  
  <div class="formulario">
   <label>E-mail:</label>
   <input type="text" name="email" title="Por favor ingresa un email valido" class="requerido" />
  </div>
  
  <div class="formulario">
   <label>Sexo:</label>
   <select name="sexo">
    <option value="M">Masculino</option>
    <option value="F">Femenino</option>
   </select>
  </div>
  
  <div class="formulario">
   <label>Departamento:</label>
   <select name="departamento" id="departamento" title="Seleccione un departamento" class="requerido">
      <option value="0"> Seleccione</option>
    <?php foreach($departamento as $item): ?>
      <option value="<?=$item->id_departamento?>"><?=$item->nombre?></option>
    <?php endforeach; ?>
   </select>
  </div>
  
   <div class="formulario" id="div-ciudad">
   <label>Ciudad:</label>
    <select name="ciudad" id="ciudad">
     <option value="0">Seleccione</option>
    </select>
  </div>
  
  <div class="formulario" id="div-ubicacion">
   <label>Ubicacion:</label>
    <select name="ubicacion" id="ubicacion">
     <option value="0">Seleccione</option>
     <option value="1">Barrio</option>
     <option value="2">Vereda</option>
   </select>
  </div>
  
   <div id="barrio" class="formulario">
   <label>Barrio:</label>
   <select id="listaBarrio" name="barrio">
     <option value="0">Seleccione </option>
   </select>
  </div>
  
  <div id="corregimiento" class="formulario">
   <label>Corregimiento:</label>
    <select id="listaCorregimiento" name="corregimiento">
    
   </select>
  </div>
  
   <div id="vereda" class="formulario">
   <label>Vereda:</label>
    <select id="listaVereda" name="vereda">
      <option value="0">Seleccione </option>
    
   </select>
  </div>
  
  
  <div class="formulario">
   <label>Estrato:</label>
   <select name="estrato" title="Seleccione el estracto" class="requerido">
     <option value="0"> Seleccione </option>
     <option value="1"> Estrato 1 </option>
     <option value="2"> Estrato 2 </option>
     <option value="3"> Estrato 3 </option>
     <option value="4"> Estrato 4 </option>
     <option value="5"> Estrato 5 </option>
     <option value="6"> Estrato 6 </option>
   </select>
  </div>
  
  <div class="formulario">
   <label>Direcci&oacute;n:</label>
   <input type="text" name="direccion"   title="Por favor indica tu direcci&oacute;n"  class="requerido" />
  </div>
  
  <div class="formulario">
   <label>Fecha de nacimiento:</label>
   <input type="text" name="fechaNacimiento"  id="datepicker" title="Por favor indica tu fecha de nacimiento"  class="requerido" />
  </div>
  
  
  <div class="formulario">
   <label>Nivel academico:</label>
   <select id="nivelAcademico" name="nivelAcademico" title="Seleccione el nivel academico" class="requerido">
   <option value="0"> Seleccione</option>
   <option value="1"> Bachiller </option>
   <option value="2"> Tecnico </option>
   <option value="3"> Tecnologo </option>
   <option value="4"> Universitario </option>
   <option value="5"> Pos-Grado </option>
   </select>
  </div>
  
  
  <div class="formulario">
   <label>Aptitud deportiva:</label>
   <input type="text" name="aptitudDeportiva" title="Por favor ingresa tu aptitud deportiva" class="requerido"/>
  </div>
  
  
  <div class="formulario">
   <label>Aptitud artistica:</label>
   <input type="text" name="aptitudArtistica" title="Por favor imgresa tu aptitud deportiva" class="requerido" />
  </div>
  
  
  <div class="formulario">
   <label>Etnia:</label>
   <select name="etnia" id="etnia" title="Seleccione su etnia" class="requerido">
      <option value="0"> Seleccione</option>
    <?php foreach($etnia as $item): ?>
      <option value="<?=$item->id_etnia?>"><?=$item->nombre?></option>
    <?php endforeach; ?>
   </select>
  </div>
  
   <div class="formulario">
   <label>Fotografia:</label>
   <input  type="file" name="foto" class="requerido"  title="Por favor adjunte una foto en .png o .gif"/>
  </div>
  
  <input type="submit" value="Registrar"  class="button-submit"/>
  <input type="hidden"  name="disponible" value="0"  id="disponible" />
  <input type="hidden"  name="registro" value="guardar"  />
</fieldset>

</form>
</div>
 </div>
 </div>
 <div class="clear"></div>
 <div id="final">ssssssss</div>
<input type="hidden" id="ruta" value="<?php echo base_url (); ?>"  />


</div>
</body>
</html>
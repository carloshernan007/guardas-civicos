<h1>Panel de busqueda </h1>

<?php echo form_open('usuario/buscarUsuario'); ?>

<fieldset>
<legend> Parametros de busqueda </legend>

<div class="formulario">
 <label> Selecciona departamento </label>
 <select name="departamento" id="departamento" class="requerido">
      <option value="0">Todos</option>
    <?php foreach($departamento as $item): ?>
      <option value="<?=$item->id_departamento?>"><?=$item->nombre?></option>
    <?php endforeach; ?>
   </select>
</div>

 <div class="formulario" id="div-ciudad">
   <label>Ciudad:</label>
    <select name="ciudad" id="ciudad">
     <option value="0">Todas</option>
    </select>
 </div>

<div class="formulario">
  <label>Perfil del usaurio</label>
   <select name="perfil" id="perfil" title="Seleccione un perfil de usuario" class="requerido">
      <option value="0"> Todos</option>
    <?php foreach($perfil as $item): ?>
      <option value="<?=$item->id_grupo?>"><?=$item->nombre?></option>
    <?php endforeach; ?>
   </select>
</div>
  
<div class="formulario">
  <label>Nombre Usuario:</label>
  <input type="text" name="nombre" />
</div>

<div class="formulario">
  <label>Apellidos:</label>
  <input type="text" name="apellido" />
</div>


<div class="formulario">
  <label>Numero de contrato:</label>
  <input type="text" name="contrato" />
</div>

<input class="button" value="Buscar" type="submit" />  
</fieldset>

<h1>Informacion del usuario </h1>

<fieldset>
<legend> Informacion de usuario </legend>
<div class="formulario extra">
  <img src="<?php echo base_url ();?>fotos/<?=$user->foto?>" />
</div>
<div class="formulario extra">
 <label> Nombre de Usuario:</label><label class="extra"><?=$user->nombreUsuario;?></label>
</div>

<div class="formulario extra"> 
 <label> Fecha de registro:</label><label class="extra"><?=$user->fecha_creacion;?></label>
</div>
 
<div class="formulario extra"> 
 <label> Perfil:</label><label class="extra"><?=$user->grupo;?></label>
</div>

<div class="formulario  extra"> 
 <label> Estado:</label><label class="extra">

  <?php if($user->estado  == 1 ) : ?>
  <a href="<?php echo base_url ();?>index.php/registro/activarUsuario/0/<?=$user->id?>">
 <img src="<?php echo base_url ();?>images/ok.png" >
 <?php else: ?>
  <a href="<?php echo base_url ();?>index.php/registro/activarUsuario/1/<?=$user->id?>">
 <img src="<?php echo base_url ();?>images/cancel.png" >
 <?php endif;?>
 </a>
</label>
</div>
</fieldset>

<fieldset>
 <legend> Información Personal</legend>
  <div class="formulario extra"> 
 <label> Numero de contrato:</label><label class="extra"><?=$user->contrato;?></label>
</div>

 
<div class="formulario extra"> 
 <label> Nombre:</label><label class="extra"><?=$user->nombre;?> <?=$user->apellido;?></label>
</div>

<div class="formulario extra"> 
 <label> Cedula:</label><label class="extra"><?=$user->cedula;?></label>
</div>

<div class="formulario extra"> 
 <label> Sexo:</label><label class="extra">
 <?php if($user->sexo == 'F'):?>
  Femenino
 <?php else: ?>
  Masculino
  <?php endif; ?>   
 </label>
</div>

<div class="formulario extra"> 
 <label> Estado Civil:</label><label class="extra"><?=$user->estado_civil?></label>
</div>

<div class="formulario extra"> 
 <label> Edad:</label><label class="extra"><?=$user->fecha_nacimiento?></label>
</div>

<div class="formulario extra"> 
 <label> Nivel Academico:</label><label class="extra"><?=$user->nivel_academico;?></label>
</div>

<div class="formulario extra"> 
 <label> Etnia:</label><label class="extra"><?=$user->etnia;?></label>
</div>

<div class="formulario extra"> 
 <label> Departamento:</label><label class="extra"><?=$user->departamento;?></label>
</div>

<div class="formulario extra"> 
 <label> Ciudad:</label><label class="extra"><?=$user->ciudad;?></label>
</div>

<div class="formulario extra"> 
 <label> Barrio:</label><label class="extra"><?=$user->barrio;?></label>
</div>

<div class="formulario extra"> 
 <label> Vereda:</label><label class="extra"><?=$user->vereda;?></label>
</div>

<div class="formulario extra"> 
 <label> Estrato:</label><label class="extra"> Estrato <?=$user->estrato;?></label>
</div>

<div class="formulario extra"> 
 <label> Direccion:</label><label class="extra"><?=$user->direccion;?></label>
</div>

<div class="formulario extra"> 
 <label> Telefono:</label><label class="extra"><?=$user->telefono;?></label>
</div>

<div class="formulario extra"> 
 <label> Celular:</label><label class="extra"><?=$user->celular;?></label>
</div>

<div class="formulario extra"> 
 <label> E-mail:</label><label class="extra"><?=$user->email;?></label>
</div>
</fieldset>



<fieldset>
<legend>Información familiar</legend>
<?php if($pariente == false): ?>
<div class="formulario extra"> 
<table border="0">
 <tr>
  <td rowspan="2"><img src="<?php echo base_url ();?>images/nada.png" /></td>
  <td>El sistema no puedo hallar  familiares de este usuario</td></tr>
 <tr><td>¿si desea agregar uno de clic en el icono adicionar ? &nbsp;&nbsp;<a href="#formularioFamiliar" class="modal"><img src="<?php echo base_url ();?>images/add_16.png" /> </a></p></td></tr>
</table>
</div>
<?php else: ?>
<table>
<thead>
  <th>#</th>
  <th>Nombre</th>
  <th>Parentesco</th>
  <th>Escolaridad</th>
  <th>Aptitud Artistica</th>
  <th>Aptitud Deportiva</th>
  <th>Telefono</th>
  <th>Fecha Nacimiento</th>
  <th>Acciones</th>
  </thead>
<?php $cont=1; foreach($pariente as $item): ?>  
<tr>
  <td><?=$cont?></td>
  <td><?=$item->nombre?> <?=$item->apellido?></td>
  <td><?=$item->parentesco?></td>
  <td><?=$item->escolaridad?></td>
  <td><?=$item->aptitud_artistica?></td>
  <td><?=$item->aptitud_deportiva?></td>
  <td><?=$item->telefono?></td>
  <td><?=$item->fecha_nacimiento?></td>
  <td class="action">
  <?php $campo['class'] = 'delete'; echo anchor("usuario/deleteFamiliar/$item->id_familiar", 'Eliminar', $campo) ; ?>
  <a href="#formularioFamiliar" class="modal edit">Nuevo</a>
  </td>
</tr>
<?php $cont++; endforeach; ?>
</table>


<?php endif; ?>

<div id="mask">
<div id="formularioFamiliar" class="window">
<fieldset>
 <legend> Registro Familiar</legend>
<?php $atributos = array('id' => 'formulario1');
echo form_open('usuario/addFamiliar',$atributos ); ?>
 <div class="formulario">
   <label>Nombre:</label>
   <input type="text" name="nombre" title="Por favor ingrese el nombre" class="required" />
 </div>
 <div class="formulario">
   <label>Apellido:</label>
   <input type="text" name="apellido" title="Por favor ingrese el apellido" class="required" />
 </div>
 <div class="formulario">
   <label>parentesco:</label>
   <select name="parentesco" class="required" title="Por favor seleccione el parentesco">
     <option value=""> Seleccione</option>
     <option value="Padre">Padre </option>
     <option value="Madre">Madre</option>
     <option value="Tio">Tio</option>
     <option value="Hermano(a)">Hermano(a)</option>
     <option value="Primo(a)">Primo(a)</option>
     <option value="Hijo(a)"> Hijo(a)</option>
     <option value="Abuelo(a)"> Abuelo(a)</option>
     <option value="Pareja">Pareja</option>
     <option value="Esposo(a)">Esposo(a)</option>
   </select>
 </div>
<div class="formulario">
   <label>Fecha de nacimiento:</label>
   <input type="text" name="fechaNacimiento"  title="Por favor indica tu fecha de nacimiento"  class="required" value="1982-11-02" />
  </div>
 <div class="formulario">
   <label>Telefono:</label>
   <input type="text" name="telefono" title="Por favor ingrese el telefono" class="required" />
 </div>  
  
  <div class="formulario">
   <label>Nivel academico:</label>
   <select id="nivelAcademico" name="nivelAcademico" title="Seleccione el nivel academico" class="required">
   <option value=""> Seleccione</option>
   <?php foreach($escolaridad as $item): ?>
   <option value="<?=$item->id?>"> <?=$item->nombre?> </option>
   <?php endforeach; ?>
   
   </select>
  </div>
  
  <div class="formulario">
   <label>Aptitud deportiva:</label>
   <input type="text" name="aptitudDeportiva" title="Por favor ingresa tu aptitud deportiva" class="required"/>
  </div>
  
  
  <div class="formulario">
   <label>Aptitud artistica:</label>
   <input type="text" name="aptitudArtistica" title="Por favor ingresa tu aptitud deportiva" class="required" />
  </div>
  
 <input type="submit" class="botton" value="Agregar"  id="nuevoBarrio"/> 
 <input type="hidden" name="url" value="<?php echo $this->uri->uri_string(); ?>" />
 <input type="hidden" name="usuario" value="<?=$id?>" />
 </fieldset>
</form> 
</div>
</div>
</fieldset>
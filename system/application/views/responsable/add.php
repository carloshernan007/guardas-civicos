<h1> Asignando Usuarios a el evento </h1>

<fieldset>
 <legend> Información del evento </legend>
  <div class="formulario extra"> 
 <label>Nombre:</label><label class="extra"><?=$evento->nombre;?></label>
</div>

  <div class="formulario extra"> 
 <label>Fecha de registro:</label><label class="extra"><?=$evento->fecha_creacion;?></label>
</div>

  <div class="formulario extra"> 
 <label>Fecha de inicio:</label><label class="extra"><?=$evento->fecha_inicio;?></label>
</div>

 <div class="formulario extra"> 
 <label>Fecha que finaliza:</label><label class="extra"><?=$evento->fecha_terminacion;?></label>
</div>

  <div class="formulario extra"> 
 <label>Realizado en la cidad:</label><label class="extra"><?=$evento->ciudad;?></label>
</div>

  <div class="formulario extra"> 
 <label>Registrado por :</label><label class="extra"><?=$evento->usuario;?></label>
</div>
</fieldset>
<br />
<?php $atrivutos = array('id' => 'formulario1');
echo form_open('responsable/add',$atrivutos ); ?>

<table>
 <thead>
  <th>#</th>
  <th>Asignar</th>
  <th>Perfil<br />
    <select name="perfil" id="perfil" class="filtro filtra">
   <option value="0">Todos</option>
      <?php foreach($perfil as $item): ?>
       <?php if($item->id_grupo == $perfil_activo): ?>
       <option value="<?=$item->id_grupo?>" selected="selected"><?=$item->nombre?></option>
       <?php else: ?>
       <option value="<?=$item->id_grupo?>" ><?=$item->nombre?></option>
       <?php endif; ?>
      <?php endforeach;?> 
      </select>
  </th>


  <th>Departamento<br>
        <select name="departamento" id="departamento" class="filtro filtra">
       <option value="0">Seleccione</option>
      <?php foreach($departamento as $item): ?>
       <?php if($item->id_departamento == $departamento_activo): ?>
       <option value="<?=$item->id_departamento?>" selected="selected"><?=$item->nombre?></option>
       <?php else: ?>
       <option value="<?=$item->id_departamento?>" ><?=$item->nombre?></option>
       <?php endif; ?>
      <?php endforeach;?> 
      </select>
  
  </th> 
  <th>Ciudad<br>
    <select name="ciudad" id="ciudad" class="filtro filtra">
       <option value="0">Seleccione</option>
         <?php foreach($ciudad as $item): ?>
             <?php if($item->id_ciudad == $ciudad_activa): ?>
           <option value="<?=$item->id_ciudad?>" selected="selected"><?=$item->nombre?></option>
            <?php else: ?>
              <option value="<?=$item->id_ciudad?>"><?=$item->nombre?></option>
             <?php endif; ?>
         <?php endforeach;?>
      </select>
  </th>
  <th>Usuario</th>
  <th>Nombre</th>
  <th>Apellido</th>
  <th>Cedula</th>
  <th>Contrato
  <input type="hidden" name="entidad" value="<?=$entidad?>"  />
  <input type="hidden" name="evento" value="<?=$evento->id_evento?>"  />
  </th>
 </thead>
 
 </form>
 
 <?php $atrivutos = array('id' => 'formulario1');
echo form_open('responsable/asigna',$atrivutos ); ?>
 <?php if($usuario != false): ?>
 <tr><td colspan="10"><input type="submit" value="Asignar" class="botton" /></td></tr>

<?php $cont=1; foreach($usuario as $item): ?> 
 <tr>
   <td><?=$cont?></td>
   <td><input type="checkbox" name="asigna[<?=$cont?>]" value="<?=$item->id?>"/> </td>
   <td><?=$item->perfil?> </td>
   <td><?=$item->departamento?></td>
   <td><?=$item->ciudad?></td>
   <td><?=$item->nombreUsuario?></td>
   <td><?=$item->nombre?> </td>
   <td><?=$item->apellido?></td>
   <td><?=$item->cedula?></td>
   <td><?=$item->contrato?></td>
 </tr>
 <?php $cont++; endforeach; ?>

   <tr><td colspan="10"><input type="submit" value="Asignar" class="botton" /></td></tr>
   <input type="hidden" name="evento" value="<?=$evento->id_evento?>"  />
   <input type="hidden" name="entidad" value="<?=$entidad?>"  />
   </form>
   
 <?php else: ?>
</table>
<fieldset>
 <legend> Información </legend>
 <div class="formulario">
  <label>El sistema no puedo hallar personal en el area </label>
 </div>
</fieldset>
<?php endif;?>

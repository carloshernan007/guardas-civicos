<h1> Lista de usuarios asignados al evento </h1>
<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>
<fieldset>
 <legend> Información de la entidad </legend>
 <div class="formulario extra"> 
 <label>Nombre:</label><label class="extra"><?=$entidad->nombre;?></label>
</div>

<div class="formulario extra"> 
 <label>Sector:</label><label class="extra"><?php if($entidad->sector == 1):?> Publico <?php else: ?> Privado <?php endif;?></label>
</div> 


<div class="formulario extra"> 
 <label>Ciudad:</label><label class="extra"><?=$entidad->ciudad;?></label>
</div> 


<div class="formulario extra"> 
 <label>Fecha de resgistro:</label><label class="extra"><?=$entidad->fecha;?></label>
</div> 
</fieldset>

<fieldset>
 <legend> Información del evento </legend>
  <div class="formulario extra"> 
 <label>Nombre:</label><label class="extra"><?=$evento->nombre;?></label>
</div>

  <div class="formulario extra"> 
 <label>Fecha de registro:</label><label class="extra"><?=$evento->fecha_creacion;?></label>
</div>

  <div class="formulario extra"> 
 <label>Fecha de inicio:</label><label class="extra"><?=$evento->fecha_inicio;?></label>
</div>

 <div class="formulario extra"> 
 <label>Fecha que finaliza:</label><label class="extra"><?=$evento->fecha_terminacion;?></label>
</div>

  <div class="formulario extra"> 
 <label>Realizado en la cidad:</label><label class="extra"><?=$evento->ciudad;?></label>
</div>

  <div class="formulario extra"> 
 <label>Registrado por :</label><label class="extra"><?=$evento->usuario;?></label>
</div>
</fieldset>


<?php if($responsable == false ): ?>

<fieldset>
  <legend> Se presento lo siguiente </legend>
<div class="formulario extra"> 
 <label>Observación:</label><label class="extra">Para este evento no se ha asignado personal</label>
</div>

 <?php $atributos = array('id' => 'formulario1');
   echo form_open('responsable/add',$atributos ); ?>
<div class="formulario extra"> 
 <label>Asignar usuarios:</label><label class="extra"><a class="envio">Registar</a>
  <input type="hidden" name="ciudad" value="<?=$evento->id_ciudad?>" />
  <input type="hidden" name="departamento" value="<?=$evento->id_departamento?>" />
  <input type="hidden" name="evento" value="<?=$evento->id_evento?>" />
  <input type="hidden" name="entidad" value="<?=$entidad->id_entidad?>" />
  <input type="hidden" name="perfil" value="0" />
 </label>
</div>
  </form>
</fieldset>

<?php else: ?>
<h1> Usuarios </h1>
<table border="1">
  <thead>
   <th>#</th>
   <th>Perfil</th>
   <th>Fecha Asignacion</th>
   <th>Usuario</th>
   <th>Nombre</th>
   <th>Apellido</th>
   <th>Celular</th>
   <th>Ciudad</th>
   <th>Foto</th>
   <th>Acciones</th>
  </thead>
  <tr>
  <td class="action"  colspan="10" >
  
     <?php $atributos = array('id' => 'formulario1');
   echo form_open('responsable/add',$atributos ); ?>
   <a class="view envio">Registar</a>
  <input type="hidden" name="ciudad" value="<?=$evento->id_ciudad?>" />
  <input type="hidden" name="departamento" value="<?=$evento->id_departamento?>" />
  <input type="hidden" name="evento" value="<?=$evento->id_evento?>" />
  <input type="hidden" name="entidad" value="<?=$entidad->id_entidad?>" />
  <input type="hidden" name="perfil" value="0" />
       </form>
	
   </td>
  </tr>
<?php $cont=1; foreach($responsable  as $item): ?>
 <tr>
   <td><?=$cont?></td>
   <td><?=$item->grupo?></td>
   <td><?=$item->fecha?></td>
   <td><?=$item->nombreUsuario?></td>
   <td><?=$item->nombre?></td>
   <td><?=$item->apellido?></td>
   <td><?=$item->celular?></td>
   <td><?=$item->ciudad?></td>
   <td><a href="<?php echo base_url (); ?>fotos/<?=$item->foto?>" target="_blank" class="modal" title="Fotografia de <?=$item->nombreUsuario?> ">Foto</a></td>
   <td class="action">
     <?php $campo['class'] = 'view';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("", 'eliminar', $campo) ; ?>
     <?php $campo['class'] = 'delete';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("", 'eliminar', $campo) ; ?>
   </td>
 </tr>
<?php $cont++; endforeach; ?>
</table>
<?php endif; ?>



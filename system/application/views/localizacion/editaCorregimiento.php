<h1> Actualizando el Corregimiento</h1>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('localizacion/actualizaCorregimiento',$atrivutos ); ?>
<fieldset>
 <legend>Información del Corregimiento</legend>
  <div class="formulario">
    <label>Ciudad: </label>
    <select name="ciudad">
    <?php foreach($ciudad as $item):?>
       <?php if($corregimiento->id_ciudad == $item->id_ciudad): ?>
          <option value="<?=$item->id_ciudad?>" selected="selected"><?=$item->nombre?></option>
        <?php else: ?>
          <option value="<?=$item->id_ciudad?>"><?=$item->nombre?></option>  
        <?php endif; ?>
    <?php endforeach; ?>
    </select>
  </div>
  <div class="formulario">
   <label> Nombre:</label>
   <input name="nombre" type="text"  value="<?=$corregimiento->nombre?>" class="required" title="Por favor ingrese el nombre del barrio"/>
  </div>
  <input type="hidden" name="id" value="<?=$corregimiento->id_corregimiento?>" />
  <input type="hidden" name="ciudadActual" value="<?=$corregimiento->id_ciudad?>" />
  <input type="submit" class="button" value="Actualizar" />
  </form>
</fieldset>
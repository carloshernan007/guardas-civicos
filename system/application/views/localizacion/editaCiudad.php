<h1> Registrando Nueva Ciudad </h1>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('localizacion/actualizaCiudad',$atrivutos ); ?>
<fieldset>
 <legend>Informaci&oacute;n de la ciudad</legend>

    <div class="formulario">
     <label>Departamento</label>
     <select name="departamento" class="required" title="Por favor seleccione el departamento">
      <?php foreach($departamento as $item): ?>
        
		<?php if($item->id_departamento == $ciudad->id_departamento): ?>
           <option value="<?=$item->id_departamento?>" selected="selected"><?=$item->nombre?></option>
        <?php endif;?>
             
       <option value="<?=$item->id_departamento?>"><?=$item->nombre?></option>
      <?php endforeach; ?>
     </select>
    </div>  
    <div class="formulario">
     <label>Nombre:</label><input name="nombre" value="<?=$ciudad->nombre?>" class="required" title="Por favor ingrese el nombre de la ciudad" />
    </div>
     <input type="hidden" name="id" value="<?=$ciudad->id_ciudad?>"  />
    <input type="submit" class="button" value="Actualizar" />
</fieldset>
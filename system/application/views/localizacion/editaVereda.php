<h1><?=$titulo?></h1>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('localizacion/actualizaVereda',$atrivutos ); ?>
<fieldset>
 <legend> Información de las veredas </legend>
 <div class="formulario">
   <label>Corregimiento:</label>
   <select name="corregimiento">
    <?php foreach($corregimiento as $item): ?>
      <?php if($vereda->id_corregimiento == $item->id_corregimiento): ?>
      <option value="<?=$item->id_corregimiento?>" selected="selected"><?=$item->nombre?></option>
      <?php else: ?>
            <option value="<?=$item->id_corregimiento?>"><?=$item->nombre?></option>
      <?php endif; ?>      
    <?php endforeach;?>
   </select>
 </div>
  <div class="formulario">
   <label>Nombre: </label>
   <input name="nombre" class="required" title="Por favor ingrese el nombre de la vereda" value="<?=$vereda->nombre?>" /> 
 </div>
 <input type="hidden" name="id" value="<?=$vereda->id_vereda?>" /
 <input type="submit" class="botton" value="Actualizar" />
 </fieldset>
</form>
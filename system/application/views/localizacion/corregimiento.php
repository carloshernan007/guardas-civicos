<h1>Lista de corregimientos de la ciudad <?=$ciudad->nombre?></h1>
<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

 <table>
 <thead>
  <th>Numero</th>
  <th>Nombre</th>
  <th>Acciones</th>
 </thead>
 
 <tr>
  <td class="action" colspan="4">
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/ciudad/$ciudad->id_departamento", 'Ciudad', $campo) ; ?>
  <?php $campo['class'] = 'view'; echo anchor("localizacion/addCorregimiento/$ciudad->id_departamento/$ciudad->id_ciudad", 'Nuevo', $campo) ; ?>
 </td>
 </tr>
<?php $cont=1; foreach($corregimiento as $item): ?> 
 <tr>
  <td><?=$cont?></td>
  <td><?=$item->nombre?></td>
  <td class="action">
  <?php $campo['class'] = 'view'; echo anchor("localizacion/vereda/$item->id_corregimiento", 'Ver', $campo) ; ?>
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/editaCorregimiento/$item->id_corregimiento/$ciudad->id_departamento", 'Editar', $campo) ; ?>
  <?php $campo['class'] = 'delete';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("localizacion/eliminaCorregimiento/$item->id_corregimiento/$ciudad->id_ciudad", 'eliminar', $campo) ; ?>
  </td>
 </tr>
<?php $cont++; endforeach; ?> 
 </table> 
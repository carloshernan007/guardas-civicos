<h1>Gestor de departamentos </h1>

<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

<table>
 <thead><th>Numero</th><th>Nombre</th><th>Acciones</th></thead>
 <tr><td colspan="4" class="action"><?php $campo['class'] = 'view'; echo anchor("localizacion/addDepartamento", 'Nuevo', $campo) ; ?> </tr>
<?php $cont=1; foreach($departamento as $item): ?>
<tr>
 <td><?=$cont;?></td>
 <td><?=$item->nombre?></td>
  <td class="action">
  <?php $campo['class'] = 'view'; echo anchor("localizacion/ciudad/$item->id_departamento", 'Ver', $campo) ; ?>
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/editaDepartamento/$item->id_departamento", 'Edit', $campo) ; ?>
  <?php $campo['class'] = 'delete'; $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";
   echo anchor("localizacion/eliminarDepartamento/$item->id_departamento", 'eliminar', $campo) ; ?>
  </td>
</tr>
<?php $cont++; endforeach; ?>
</table>


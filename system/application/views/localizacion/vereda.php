<h1><?=$titulo?></h1>

<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

 <table>
 <thead>
  <th>Numero</th>
  <th>Nombre</th>
  <th width="40%">Acciones</th>
 </thead>
<tr>
 <td colspan="4"  class="action">
 <?php $campo['class'] = 'edit'; echo anchor("localizacion/corregimiento/$corregimiento->id_ciudad", 'Corregimientos', $campo) ; ?>
 <?php $campo['class'] = 'view'; echo anchor("localizacion/addVereda/$corregimiento->id_ciudad/$corregimiento->id_corregimiento", 'Nueva', $campo) ; ?> 
 </td>
</tr>
<?php if($vereda != false) : ?>

  <?php $cont=1; foreach($vereda as $item): ?>
<tr>
  <td><?=$cont?></td>
  <td><?=$item->nombre?></td>
  <td class="action">
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/editaVereda/$item->id_vereda/$corregimiento->id_ciudad", 'Editar', $campo) ; ?>
  <?php $campo['class'] = 'delete';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("localizacion/eliminarVereda/$item->id_vereda/$corregimiento->id_corregimiento/", 'eliminar', $campo) ; ?>
  </td>
</tr>   
  <?php $cont++;endforeach; ?> 
  
<?php else: ?>
<tr><td>No se han encontrado veredas en este corregimiento </td></tr>
<?php endif; ?>

 
</table>
<h1> Registrando Barrios a la ciudad <?=$ciudadActual->nombre?> </h1>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('localizacion/registraCorregimiento',$atrivutos ); ?>
<fieldset>
 <legend> Información de el Corregimiento </legend>
 <div class="formulario">
   <label>Ciudad:</label>
   <select name="ciudad">
    <?php foreach($ciudad as $item): ?>
      <?php if($ciudadActual->id_ciudad == $item->id_ciudad): ?>
      <option value="<?=$item->id_ciudad?>" selected="selected"><?=$item->nombre?></option>
      <?php else: ?>
            <option value="<?=$item->id_ciudad?>"><?=$item->nombre?></option>
      <?php endif; ?>      
    <?php endforeach; ?>
   </select>
 </div>
 <div class="formulario">
   <label>Nombre: </label>
   <input name="nombre[0]" class="required" title="Por favor ingrese el nombre del corregimiento" /> 
 </div>
 <div id="contenedor1"></div>
 
 <input type="button" class="botton" value="Agregar"  id="nuevoBarrio"/>
 <input type="hidden" id="contador" value="1" >
 <input type="submit" class="botton" value="Registrar" />
</fieldset>

</form>
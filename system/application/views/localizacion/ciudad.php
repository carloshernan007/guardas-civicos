<h1> Ciudades del departamento <?=$departamento->nombre;?></h1>

<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

<table>
 <thead>
  <th>Numero</th>
  <th>Nombre</th>
  <th width="40%">Acciones</th>
 </thead>
<tr><td colspan="4"  class="action">
<?php $campo['class'] = 'edit'; echo anchor("localizacion", 'Departamentos', $campo) ; ?>
 <?php $campo['class'] = 'view'; echo anchor("localizacion/addCiudad/$departamento->id_departamento", 'Nueva', $campo) ; ?> </td></tr>

<?php if($ciudad != false): ?>
<?php $cont=1; foreach($ciudad as $item): ?>
<tr>
 <td><?=$cont?></td>
 <td><?=$item->nombre?></td>
 <td class="action">
  <?php $campo['class'] = 'view'; echo anchor("localizacion/barrio/$item->id_ciudad", 'Barrio', $campo) ; ?>&nbsp;
   <?php $campo['class'] = 'view'; echo anchor("localizacion/Corregimiento/$item->id_ciudad", 'Corregimiento', $campo) ; ?>
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/editaCiudad/$item->id_ciudad", 'Edit', $campo) ; ?>
  <?php $campo['class'] = 'delete';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("localizacion/eliminarCiudad/$item->id_ciudad/$departamento->id_departamento", 'eliminar', $campo) ; ?>
  </td>
</tr>
<?php $cont++; endforeach; ?>
<?php else: ?>

<tr><td colspan="4">No se han registrados Ciudades para el departamento <?=$departamento->nombre;?>, para registrar de clic en Nueva </td></tr> 
<?php endif; ?> 
</table>
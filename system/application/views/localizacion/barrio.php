<h1>Lista de barrios Ciudad de <?=$ciudad->nombre?></h1>


<div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>

 <table>
 <thead>
  <th>Numero</th>
  <th>Nombre</th>
  <th>Comuna<br />
    <?php $atrivutos = array('id' => 'formulario1');
echo form_open("localizacion/barrio/$id",$atrivutos ); ?>
     <select name="comuna" id="comuna">
     <option value=''> Seleccione </option>
      <option value="0"> Todas </option>
      <?php for($i = 1; $i <= 21 ; $i++): ?>
      <option value="<?=$i?>"> Comuna <?=$i?> </option>
      <?php endfor; ?>
     </select>
    </form> </th>
  <th width="25%">Acciones</th>
 </thead>
  
<tr>
 <td class="action" colspan="4">
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/ciudad/$ciudad->id_departamento", 'Ciudad', $campo) ; ?>
  <?php $campo['class'] = 'view'; echo anchor("localizacion/addBarrio/$ciudad->id_departamento/$ciudad->id_ciudad", 'Nuevo', $campo) ; ?>
 </td>
</tr>
<?php if($barrio != false): ?>
   
 <?php $cont=1; foreach($barrio as $item): ?>
  <tr>
    <td><?=$cont?></td>
    <td><?=$item->nombre?></td>
    <td>Comuna <?=$item->id_comuna?></td>
    <td class="action">
  <?php /*$campo['class'] = 'view'; echo anchor("localizacion/guardas/$item->id_barrio", 'Ver', $campo) ;*/ ?>
  <?php $campo['class'] = 'edit'; echo anchor("localizacion/editaBarrio/$item->id_barrio/$ciudad->id_departamento", 'Editar', $campo) ; ?>
  <?php $campo['class'] = 'delete';  $campo['rel'] = "Esta seguro de desea eliminar $item->nombre ?";echo anchor("localizacion/eliminaBarrio/$item->id_barrio/$ciudad->id_ciudad", 'eliminar', $campo) ; ?>
  </td>
  </tr>
 <?php $cont++; endforeach; ?>
<?php else: ?>

<tr>
 <td colspan="4"> No se encontraron Barrios en esta ciudad o comuna</td>
</tr> 
<?php endif; ?>
   </table>



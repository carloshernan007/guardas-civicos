<h1> Actualizando el barrio </h1>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('localizacion/actualizaBarrio',$atrivutos ); ?>
<fieldset>
 <legend>Información del Barrio</legend>
  <div class="formulario">
    <label>Ciudad: </label>
    <select name="ciudad">
    <?php foreach($ciudad as $item):?>
       <?php if($barrio->id_ciudad == $item->id_ciudad): ?>
          <option value="<?=$item->id_ciudad?>" selected="selected"><?=$item->nombre?></option>
        <?php else: ?>
          <option value="<?=$item->id_ciudad?>"><?=$item->nombre?></option>  
        <?php endif; ?>
    <?php endforeach; ?>
    </select>
  </div>
  
  <div class="formulario">
   <label>Comuna:</label>
       <select name="comuna">
   <?php for($i=1;$i < 17; $i++):?>
         <?php if($barrio->id_comuna == $i): ?>
          <option value="<?=$i?>" selected="selected"> Comuna <?=$i?></option>
        <?php else: ?>
          <option value="<?=$i?>">Comuna<?=$i?></option>  
        <?php endif; ?>  
   <?php endfor;?>
       </select>
  </div>
  
  <div class="formulario">
   <label> Nombre:</label>
   <input name="nombre" type="text"  value="<?=$barrio->nombre?>" class="required" title="Por favor ingrese el nombre del barrio"/>
  </div>
  <input type="hidden" name="id" value="<?=$barrio->id_barrio?>" />
  <input type="hidden" name="ciudadActual" value="<?=$barrio->id_ciudad?>" />
  <input type="submit" class="button" value="Actualizar" />
  </form>
</fieldset>
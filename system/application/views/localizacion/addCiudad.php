<h1> Registrando Nueva Ciudad </h1>

<?php $atrivutos = array('id' => 'formulario1');
echo form_open('localizacion/registraCiudad',$atrivutos ); ?>
<fieldset>
 <legend>Informaci&oacute;n nueva ciudad</legend>
 <?php if($departamento != false): ?>
    <div class="formulario">
     <label>Departamento</label>
     <select name="departamento" class="required" title="Por favor seleccione el departamento">
      <option value="">Seleccione</option>
      <?php foreach($departamento as $item): ?>
       <option value="<?=$item->id_departamento?>"><?=$item->nombre?></option>
      <?php endforeach; ?>
     </select>
    </div>
 <?php else: ?>
  <input type="hidden" value="<?=$id?>" name="departamento" >
 <?php endif; ?>   
    <div class="formulario">
     <label>Nombre:</label><input name="nombre" class="required" title="Por favor ingrese el nombre de la ciudad" />
    </div>
    
    <input type="submit" class="button" value="Registrar" />
</fieldset>
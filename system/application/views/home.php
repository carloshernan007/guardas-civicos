<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$estado->nombre_sitio?></title>
<link href="<?php  echo base_url (); ?>css/transdmin.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url (); ?>js/funciones.js"></script>
</head>

<body>
<div id="contenedor">
    <div class="conformacion">
<?php echo $this->session->flashdata('mensaje');?>
</div>
    <div id="loginPan">
		<h2>Bienvenido<span>Usuario</span></h2>
    <?php $atributos = array('id' => 'formulario1');
echo form_open('actividad/login',$atributos ); ?>          
	
		<label>Usuario </label><input name="usuario" type="text"  class="required" />
		<label>Clave</label><input name="clave" type="password" class="required" />
		<input name="Input" type="submit" class="button" value="Iniciar" />
		</form>
        <?php if ($estado->registro_usuarios == 1) : ?>
		<ul>
			<li class="nonregister">No estas registrado?</li>
			<li class="register"><a href="<?php echo base_url (); ?>index.php/registro/registroUsuario" >Registrate</a> </li>
		</ul>
        <?php endif; ?> 
	</div>
    
<div style=" clear:both"> </div>    
</div>
</body>
</html>
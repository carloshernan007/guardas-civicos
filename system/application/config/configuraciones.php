<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
*  Archivo de configuracion :  En este archivo se definen reglas para la logica del negocio de la apliacion
*  Fecha: 28 de febrero del 2010
*/
 
/*******************************************************************************************
  Reglas para la  subir las fotos de los usuarios del portal 
*******************************************************************************************/
$config['subirFotos'] = array(
							    array('upload_path' => './fotos/'),
							    array('allowed_types ' => 'jpg|png'),
								array('max_size' => 1000),
								array('max_width' => 300),
								array('max_height' => 300), 
							  );
/**
* Funciones        : Funciones para ejectos de registro y ajax
* Fecha            : 24 de febrero del 2010
* Ingeniero        :
*/

/******************************************************************************************
  Funcion que actualiza la lista de ciudades por ajax
******************************************************************************************/
$(document).ready(function (){
 $("#departamento").change(function (){
  var departamento = $(this).val (); 
  var ruta = $('#ruta').val ();
  ruta += 'index.php/registro/ciudad/'+ departamento;
    $.post( ruta,{departamento : departamento},
		     function(data){
			   $('#ciudad').html(data);  
		      }
		   );
  });
});
/******************************************************************************************
 Funcion que se encarga de los efectos de la localizacion del usuario
******************************************************************************************/
$(document).ready(function (){ 
$('#div-ciudad,#div-ubicacion,#barrio,#vereda,#corregimiento').hide ();
$('#departamento').change(function(){ $('#div-ciudad').show('show')});
$('#ciudad').change(function(){ $('#div-ubicacion').show('show')});

});

/******************************************************************************************
  Funcion que oculta los campos de barrio y Corregimiento
******************************************************************************************/
$(document).ready(function (){
  $('#vereda').hide ();
  $("#ubicacion").change(function (){
   var ciudad = $('#ciudad').val ();
   var ruta = $('#ruta').val ();  
   if($("#ubicacion").val () == 1)
    {
	  $('#vereda,#corregimiento').hide ("show");
	  $('#barrio').show("show");
      ruta += 'index.php/registro/listaBarrios/' + ciudad; 
	   $.post( ruta,{id : 'huy'},
		     function(data){
			   $('#listaBarrio').html(data);  
		      }
		   );
	}
   else
   {
	 $('#barrio').hide ("show");
	 $('#corregimiento').show("show");
	 ruta += 'index.php/registro/listaCorregimiento/' + ciudad; 
	   $.post( ruta,{id : 'huy'},
		     function(data){
			   $('#listaCorregimiento').html(data);  
		      }
		   );
   }

  });
/*******************************************************************************************
 Funcion que lista las veredas
*******************************************************************************************/
$("#listaCorregimiento").change(function (){
   var corregimiento = $('#listaCorregimiento').val ();
   var ruta = $('#ruta').val ();   
       ruta += 'index.php/registro/listaVereda/' + corregimiento; 
	    $.post( ruta,{id : 'huy'},
		     function(data){
			   $('#listaVereda').html(data);  
		      }
		   );
   $('#vereda').show("show");		
   });


});

/******************************************************************************************
Funcion que  genera el calendario 
********************************************************************************************/
$(document).ready(function (){
$(function() {
		$('#datepicker').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1900:2010',
			dateFormat: 'yy-mm-dd'
		});
	});
$("#ui-datepicker-div").hide ();
});
/******************************************************************************************
Funcion que realiza las validaciones del formulario de usuario
******************************************************************************************/
function validacion()
{
  var cont = 0;	
  var camposInput = new Array('nombreUsuario','claveUsuario','confirmaClave','contrato','nombre','apellido','cedula','telefono',
							  'celular','email','fechaNacimiento','aptitudDeportiva','aptitudArtistica','foto','direccion');	
  var camposSelect = new Array('perfil','grupoSanguineo','departamento','ciudad','estrato', 'nivelAcademico','etnia','estadoCivil');
  cont += recoridoInput(camposInput);
  cont +=  recoridoSelect(camposSelect);
  var campo = $("input[name='nombreUsuario']"); 
  if($("#disponible").val () == 0)
   {
     cont += 1;
	 campo.after('<label class="error" generated="true" for="'+ campo.attr('name') +'">El nombre de usuario <b>'+ campo.val() +'</b> esta en uso </label>'); 
   }
 
 if (cont != 0)
  return false;
   
	
}
/*****************************************************************************************
 Funcion que recore los select del  formulario
*****************************************************************************************/
function recoridoSelect (campos)
{
  var cont = 0;
   for(var i = 0; i < campos.length ; i++) {
	 cont+= selecVacio(campos[i]); 
	 
   }
  return cont;
    
}
/*****************************************************************************************
 Funcion que recore los input de  formulario
*****************************************************************************************/
function recoridoInput (campos)
{
  var cont = 0;
   for(var i = 0; i < campos.length ; i++) {
	 cont+= campoVacio(campos[i]); 
	 
   }
  return cont;   
}
/*****************************************************************************************
Funcion que valida los campos en blanco 
******************************************************************************************/
function campoVacio(campo)
{
  var campo = $("input[name='"+campo+"']");	
  if(campo.val () == '') {
	if(!campo.next().is('label')){
	   campo.after('<label class="error" generated="true" for="' + campo.attr('name') +'"> '+ campo.attr('title') + ' </label>'); 
	   campo.addClass('error');
	   return  1;
	 }
   }
  else
   {
	   campo.removeClass('error')
	   $("label[for='"+ campo.attr('name') +"']").remove();
   }
  if(campo.attr('name') == 'contrato' || campo.attr('name') == 'cedula' || campo.attr('name') == 'celular' )
   {
	 if(isNaN(campo.val ())){
	  if(!campo.next().is('label')){
	   campo.after('<label class="error" generated="true" for="' + campo.attr('name') +'"> '+ campo.attr('title') + ' </label>'); 
	   campo.addClass('error');
	   return  1;
	  }
	}  
   }
   if(campo.attr('name') == 'email')
    {
	  if(!(/\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test(campo.val ())))
	   {
		 if(!campo.next().is('label')){
	      campo.after('<label class="error" generated="true" for="' + campo.attr('name') +'"> '+ campo.attr('title') + ' </label>'); 
	      campo.addClass('error');
	      return  1;  }
	   }																		
    }
   
   return 0;
}
/******************************************************************************************
Funcion que  valida  los arreglos select
******************************************************************************************/
function selecVacio (campo)
{	
  var  campo = $("select[name='"+campo+"']");	

  if( parseInt( campo.val ()) == 0)
   {
	 if( !campo.next().is('label')){  
	   campo.after('<label class="error" generated="true" for="' + campo.attr('name') +'">'+ campo.attr('title') + '</label>');   
	   campo.addClass('error');
	   return 1;
	 }
   }
  else
   {
	  campo.removeClass('error')
	  $("label[for='"+ campo.attr('name') +"']").remove();   
	  return 0;
   }
   
}
/******************************************************************************************
 Funcion que valida los campos del formulario
******************************************************************************************/
$(document).ready(function (){

$("#formulario  input[class='requerido']").blur(function () {
  if($(this).val () == '')
   {
	if( !$(this).next().is('label')){
	   $(this).after('<label class="error" generated="true" for="' + $(this).attr('name') +'"> '+ $(this).attr('title') + ' </label>'); 
	   $(this).addClass('error')
	 }
   }
  else
   {
	   $(this).removeClass('error')
	   $("label[for='"+ $(this).attr('name') +"']").remove();
   }
  
  if($(this).attr('name') == 'confirmaClave' & $(this).val () != '')
    {
	  if( $(this).val () != $("#formulario input[name='claveUsuario']").val () )	
	  {
		 $(this).after('<label class="error" generated="true" for="' + $(this).attr('name') +'">La contrase&ntilde;a no concuerdan</label>');   
		 $(this).addClass('error')
	  }	
	  else
	  {
		$(this).removeClass('error')
	    $("label[for='"+ $(this).attr('name') +"']").remove(); 
	  }
	}  
});






/********************************************************************************************
  Funcion que valida todos los selec del formulario
********************************************************************************************/
$("#formulario  select[class='requerido']").blur(function() {
  if( $(this).val () == 0)
   {
	 if( !$(this).next().is('label')){  
	   $(this).after('<label class="error" generated="true" for="' + $(this).attr('name') +'">'+ $(this).attr('title') + '</label>');   
	   $(this).addClass('error');
	 }
   }
  else
   {
	  $(this).removeClass('error')
	  $("label[for='"+ $(this).attr('name') +"']").remove();   
   }
  
});
/********************************************************************************************
 Funcion que verifica si el nombre de un usario esta disponible
********************************************************************************************/
$("#formulario  input[name='nombreUsuario']").blur(function() {
	if($(this).val () != '')
	 {	
		 var ruta = $('#ruta').val ();
		 var temp  = ruta +  'images/ajax-loader.gif';
		 var disponible;
		 $(this).after('<img src="'+ temp +'" class="error" />'); 
		 ruta += 'index.php/registro/disponibilidad/' + $(this).val(); 
		 $.post( ruta,{ nombre : 'nada'},
		     function(data){
				var campo = $("input[name='nombreUsuario']"); 
				$("img[class='error']").remove (); 
			    if(parseInt(data) == 0){
				 //  Estado del nombre ocupado
				 campo.after('<label class="error" generated="true" for="'+ campo.attr('name') +'">El nombre de usuario <b>'+ campo.val() +'</b> esta en uso </label>'); 
				 disponible = 0;
				}else{
				  $("label[for='"+ campo.attr('name') +"']").remove();
				  disponible =1;
				}
				 $("#disponible").val(disponible); 
			  }
		   );
		
		
	 }
});
/*****************************************************************************************
 Funcion para ocultar el mensaje de error
*****************************************************************************************/
$("input[name='nombreUsuario']").click(function () {
   $("div.conformacion").hide ('show');												  
});
/*****************************************************************************************
 Funcion que conforma la eliminacion de un archivo
*****************************************************************************************/
$("a[class='delete']").click(function (){
  return confirm( $(this).attr('rel'));										
});
/*****************************************************************************************
 Funcion para valiar el nuevo departamento
*****************************************************************************************/
$("#formulario1").validate ();
/****************************************************************************************
 Funcion que filtra los barrios
****************************************************************************************/
$("#comuna, .filtra").change(function (){
	$("#formulario1").submit ();					   
});
/****************************************************************************************
 Funcion que filtra los barrios
****************************************************************************************/
$(".envio").click(function (){
	$("#formulario1").submit ();					   
});
/****************************************************************************************
 Funcion que filtra los barrios
****************************************************************************************/
$("select[name='ciudades'], #sector ").change(function (){
	$("#formulario1").submit ();					   
});
/*****************************************************************************************
Funcion que agrega mas campos para el registro de barrios
*****************************************************************************************/
$("#nuevoBarrio").click(function (){
 var indice =  $("#contador").val ();
 $("#contenedor1").after('<div class="formulario"><label>Nombre: </label><input name="nombre['+ indice +']" class="required" title="Esta campo es requerido" /></div>');
 indice++;
 $("#contador").val (indice);
});
/****************************************************************************************
Funcion para las ventanas modales 
****************************************************************************************/
$("a.modal").fancybox({
				         'titleShow'		: true
			         });

});

/********************************************************************************************
 Efecto para las ventanas modales
********************************************************************************************/
$(document).ready(function() {	

	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			
	
});


-- phpMyAdmin SQL Dump
-- version 2.11.9.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-05-2010 a las 15:32:43
-- Versión del servidor: 5.0.67
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `guardacivico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accion`
--

CREATE TABLE IF NOT EXISTS `accion` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `accion`
--

INSERT INTO `accion` (`id`, `nombre`) VALUES
(1, 'welcome/index'),
(2, 'registro/registroUsuario '),
(3, 'localizacion/index '),
(4, 'localizacion/ciudad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE IF NOT EXISTS `actividad` (
  `id_actividad` int(11) NOT NULL auto_increment,
  `id_evento` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_actividad`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `actividad`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barrio`
--

CREATE TABLE IF NOT EXISTS `barrio` (
  `id_barrio` int(11) NOT NULL auto_increment,
  `id_comuna` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_barrio`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `barrio`
--

INSERT INTO `barrio` (`id_barrio`, `id_comuna`, `id_ciudad`, `nombre`) VALUES
(1, 16, 3, 'No Aplica'),
(2, 16, 3, 'Vallado'),
(3, 16, 3, 'Comuneros I'),
(4, 16, 3, 'El Retiro'),
(5, 1, 3, 'prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE IF NOT EXISTS `carrera` (
  `id_carrera` int(11) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_carrera`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `carrera`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE IF NOT EXISTS `ciudad` (
  `id_ciudad` int(11) NOT NULL auto_increment,
  `nombre` varchar(200) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  PRIMARY KEY  (`id_ciudad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcar la base de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `nombre`, `id_departamento`) VALUES
(7, 'jamundi', 1),
(6, 'Bolo', 1),
(3, 'Cali', 1),
(4, 'Buga', 1),
(5, 'Buenaventura', 1),
(8, 'Medellin', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE IF NOT EXISTS `comuna` (
  `id_comuna` int(11) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_comuna`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcar la base de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`id_comuna`, `nombre`) VALUES
(1, 'Comuna 1'),
(2, 'Comuna 2'),
(3, 'Comuna 3'),
(4, 'Comuna 4'),
(5, 'Comuna 5'),
(6, 'Comuna 6'),
(7, 'Comuna 7'),
(8, 'Comuna 8'),
(10, 'Comuna 9'),
(11, 'Comuna 10'),
(12, 'Comuna 11'),
(13, 'Comuna 12'),
(14, 'Comuna 13'),
(15, 'Comuna 14'),
(16, 'Comuna 15'),
(17, 'Comuna 16'),
(18, 'Comuna 17'),
(19, 'Comuna 18'),
(20, 'Comuna 19'),
(21, 'Comuna 20'),
(22, 'Comuna 21'),
(23, 'Comuna 21'),
(24, 'Comuna 22'),
(25, 'Comuna 23'),
(26, 'Comuna 24'),
(27, 'Comuna 25'),
(28, 'Comuna 26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id` int(11) NOT NULL auto_increment,
  `nombre_sitio` varchar(250) NOT NULL,
  `registro_usuarios` tinyint(4) NOT NULL,
  `nivel_predeterminado` int(11) NOT NULL,
  `estado_sitio` tinyint(4) NOT NULL,
  `email` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `nombre_sitio`, `registro_usuarios`, `nivel_predeterminado`, `estado_sitio`, `email`) VALUES
(1, 'Aplicacion Guardas civicos', 1, 2, 1, 'pruebas@pruebas.comoo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `corregimiento`
--

CREATE TABLE IF NOT EXISTS `corregimiento` (
  `id_corregimiento` int(11) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  PRIMARY KEY  (`id_corregimiento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcar la base de datos para la tabla `corregimiento`
--

INSERT INTO `corregimiento` (`id_corregimiento`, `nombre`, `id_ciudad`) VALUES
(1, 'El Hormigero', 3),
(2, 'El Saladito', 3),
(3, 'Felidia', 3),
(4, 'Golondrinas', 3),
(5, 'La Castilla', 3),
(6, 'La Elvira', 3),
(7, 'La Leonera', 3),
(8, 'La Paz', 3),
(9, 'Los Andes', 3),
(10, 'Montebello', 3),
(11, 'Navarro', 3),
(12, 'Pance', 3),
(13, 'Pichindé', 3),
(14, 'Villacarmelo', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `id_departamento` int(11) NOT NULL auto_increment,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY  (`id_departamento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcar la base de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `nombre`) VALUES
(1, 'Valle del cauca'),
(2, 'Vichada'),
(3, 'Meta'),
(5, 'Bolivar'),
(6, 'Casanare'),
(7, 'Putumayo'),
(8, 'Guajira'),
(9, 'Antioquia'),
(10, 'Quindio'),
(11, 'Archipielago de san andres, providencia y santa catalina'),
(12, 'Caqueta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidad_solicitante`
--

CREATE TABLE IF NOT EXISTS `entidad_solicitante` (
  `id_entidad` int(11) NOT NULL auto_increment,
  `nombre` varchar(200) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `sector` int(11) NOT NULL COMMENT '1 publicas 0 privadas',
  `fecha` datetime NOT NULL,
  PRIMARY KEY  (`id_entidad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `entidad_solicitante`
--

INSERT INTO `entidad_solicitante` (`id_entidad`, `nombre`, `id_ciudad`, `sector`, `fecha`) VALUES
(1, 'prueba aaaaa', 3, 0, '2010-03-11 07:30:13'),
(2, 'Prueba4', 3, 1, '2010-03-11 07:30:13'),
(3, 'Prueba3', 3, 1, '2010-03-11 07:30:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE IF NOT EXISTS `eps` (
  `id_eps` int(11) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_eps`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `eps`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escolaridad`
--

CREATE TABLE IF NOT EXISTS `escolaridad` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `escolaridad`
--

INSERT INTO `escolaridad` (`id`, `nombre`) VALUES
(1, 'Bachiller'),
(2, 'Tecnico'),
(3, 'Univeritario'),
(4, 'Tecnologo'),
(5, 'Básica primaria'),
(6, 'Preescolar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio`
--

CREATE TABLE IF NOT EXISTS `estudio` (
  `id_estudio` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `universidad` varchar(100) NOT NULL,
  `fecha_grado` date NOT NULL,
  `semestres` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_estudio`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `estudio`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etnia`
--

CREATE TABLE IF NOT EXISTS `etnia` (
  `id_etnia` int(11) NOT NULL auto_increment,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_etnia`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcar la base de datos para la tabla `etnia`
--

INSERT INTO `etnia` (`id_etnia`, `nombre`) VALUES
(1, 'Mestizo'),
(2, 'Negro'),
(5, 'Blanco'),
(6, 'Indio'),
(7, 'Mulato'),
(8, 'Indio negro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `evento` (
  `id_evento` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_terminacion` date NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_entidad` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  PRIMARY KEY  (`id_evento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `evento`
--

INSERT INTO `evento` (`id_evento`, `nombre`, `id_usuario`, `fecha_creacion`, `fecha_inicio`, `fecha_terminacion`, `id_tipo`, `id_entidad`, `id_ciudad`) VALUES
(1, 'Rumba 1', 1, '2010-03-13 18:26:40', '2010-03-27', '2010-03-28', 1, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiencia`
--

CREATE TABLE IF NOT EXISTS `experiencia` (
  `id_experiencia` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `empresa` varchar(200) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL,
  `sector` varchar(100) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  PRIMARY KEY  (`id_experiencia`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `experiencia`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familiar`
--

CREATE TABLE IF NOT EXISTS `familiar` (
  `id_familiar` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `registro` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `parentesco` varchar(50) NOT NULL,
  `aptitud_deportiva` varchar(100) NOT NULL,
  `aptitud_artistica` varchar(100) NOT NULL,
  `id_escolaridad` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `telefono` varchar(30) default NULL,
  PRIMARY KEY  (`id_familiar`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `familiar`
--

INSERT INTO `familiar` (`id_familiar`, `id_usuario`, `registro`, `nombre`, `apellido`, `fecha_nacimiento`, `parentesco`, `aptitud_deportiva`, `aptitud_artistica`, `id_escolaridad`, `fecha_creacion`, `telefono`) VALUES
(1, 6, 1, 'Carlos Hernan', 'aguilar', '1982-11-02', 'Padre', 'Baloncesto', 'Ninguna', 3, '2010-05-23 10:50:28', '351424145'),
(2, 6, 1, 'Carmen', 'Santamaria', '1982-11-02', 'Primo(a)', 'Futbol', 'Ninguna', 2, '2010-05-23 11:34:54', '3251511');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `id_grupo` int(11) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_grupo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id_grupo`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Guarda Civico'),
(6, 'Coordinador de Zona'),
(5, 'Interventor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE IF NOT EXISTS `permiso` (
  `id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `accion_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `permiso`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `contrato` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `cedula` int(11) NOT NULL,
  `sexo` char(1) NOT NULL,
  `tipo_sangre` varchar(5) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `estado_civil` varchar(20) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `telefono` int(11) NOT NULL,
  `celular` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `id_barrio` int(11) default NULL,
  `id_vereda` int(11) default NULL,
  `estrato` int(11) NOT NULL,
  `nivel_academico` varchar(50) NOT NULL,
  `aptitud_deportiva` varchar(20) NOT NULL,
  `aptitud_artistica` varchar(100) NOT NULL,
  `etnia` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL COMMENT 'permi registrar el numero de contrato de los usuarios que puede ser nulo',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcar la base de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `id_usuario`, `contrato`, `nombre`, `apellido`, `cedula`, `sexo`, `tipo_sangre`, `fecha_nacimiento`, `estado_civil`, `direccion`, `telefono`, `celular`, `email`, `id_departamento`, `id_ciudad`, `id_barrio`, `id_vereda`, `estrato`, `nivel_academico`, `aptitud_deportiva`, `aptitud_artistica`, `etnia`, `foto`) VALUES
(13, 6, 1, 'liliana ', 'lopez', 31305348, 'M', 'O+', '1991-03-05', 'soltero', 'calle14 12-13', 6548791, 2147483647, 'liliana@hotmail.com', 1, 3, 1, 1, 1, '1', 'futbol', 'teatro', 5, 'art_11.png'),
(14, 7, 2, 'carlos ', 'paredes', 64587920, 'M', 'AB-', '1983-01-04', 'separado', 'carrera 2 23-45', 54654455, 2147483647, 'dhireiif@hotmail.com', 1, 3, 3, 1, 3, '4', 'sosbol', 'musica', 8, 'art_08.png'),
(15, 8, 3, 'sandra', 'avila', 66948608, 'F', 'B+', '1982-04-05', 'union', 'carrea 2 98- 52', 659548, 321485569, 'nsdfjfjski@hotmail.com', 1, 3, 4, 1, 2, '2', 'fhdends', 'hdfuisrhey', 6, 'art_111.png'),
(16, 9, 25, 'julaio', 'marin', 6654896, 'M', 'O+', '1992-05-12', 'soltero', 'calle45 6- 23', 65444454, 2147483647, 'julion@hotmail.com', 1, 3, 1, 1, 2, '3', 'deporte', 'danzas', 2, 'art_06.png'),
(17, 10, 23, 'liliana', 'vente', 66459231, 'F', 'AB+', '2010-03-11', 'separado', 'calle5 8-9', 6635489, 2147483647, 'lilianav@gmail.com', 1, 3, 4, 1, 2, '4', 'futbol', 'teatro', 1, 'art_03.png'),
(18, 11, 26, 'leo', 'davila', 66321514, 'M', 'O+', '2010-03-01', 'separado', 'calle 12 9- 4', 635412654, 64856123, 'ejmdjdfod@hotmail.com', 1, 3, 2, 1, 1, '1', 'futbol', 'danzas', 5, 'art_05.png'),
(19, 12, 1234515648, 'bernardo', 'varela', 66325255, 'M', 'O+', '1994-03-02', 'soltero', 'calle2 5 -4', 36215485, 132555498, 'jdfjdf@fdjhdf.com', 1, 3, 2, 1, 2, '2', 'duhdf', 'dbfhdsgfd', 8, 'art_061.png'),
(20, 14, 1111111, 'Sara ', 'Sara', 51515151, 'F', 'A-', '1994-05-16', 'soltero', 'Sara', 11551511, 1551515, 'Sara@Sara.com', 1, 3, 4, 0, 3, '4', 'Sara', 'Sara', 7, 'store_logo.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `id` int(11) NOT NULL auto_increment,
  `id_subactividad` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `registro` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `personal`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsable`
--

CREATE TABLE IF NOT EXISTS `responsable` (
  `id_responsable` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY  (`id_responsable`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Esta tabla contiene la lista de responsables de cada evento' AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `responsable`
--

INSERT INTO `responsable` (`id_responsable`, `id_usuario`, `id_evento`, `fecha`) VALUES
(1, 6, 1, '2010-03-28 03:08:07'),
(2, 7, 1, '2010-03-28 03:08:07'),
(3, 8, 1, '2010-03-29 08:06:00'),
(4, 11, 1, '2010-03-29 08:06:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subactividad`
--

CREATE TABLE IF NOT EXISTS `subactividad` (
  `id_subactividad` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  PRIMARY KEY  (`id_subactividad`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `subactividad`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_evento`
--

CREATE TABLE IF NOT EXISTS `tipo_evento` (
  `id_tipo` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY  (`id_tipo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `tipo_evento`
--

INSERT INTO `tipo_evento` (`id_tipo`, `nombre`) VALUES
(1, 'pruebas 1'),
(2, 'pruebas 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uniforme`
--

CREATE TABLE IF NOT EXISTS `uniforme` (
  `id_uniforme` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `observacion` varchar(250) NOT NULL,
  `talla_pantalon` int(11) NOT NULL,
  `talla_camisa` varchar(3) NOT NULL,
  `tallla_kepis` varchar(10) NOT NULL,
  PRIMARY KEY  (`id_uniforme`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `uniforme`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL auto_increment,
  `estado` tinyint(4) NOT NULL default '1' COMMENT 'Estado del usuario activo o desactivado',
  `nombre` varchar(100) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_grupo` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcar la base de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `estado`, `nombre`, `clave`, `fecha_creacion`, `id_grupo`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2010-02-23 14:59:42', 1),
(6, 0, 'elizabeth ', '202cb962ac59075b964b07152d234b70', '2010-03-02 03:43:01', 2),
(7, 1, 'wiliams', 'c8ffe9a587b126f152ed3d89a146b445', '2010-03-02 03:46:35', 6),
(8, 1, 'jose', '7cec85c75537840dad40251576e5b757', '2010-03-02 03:49:41', 2),
(9, 1, 'Antioquiay', '250cf8b51c773f3f8dc8b4be867a9a02', '2010-03-02 03:52:34', 6),
(10, 1, 'liliana', '68053af2923e00204c3ca7c6a3150cf7', '2010-03-02 03:54:54', 6),
(11, 1, 'leonardo', 'cb3ce9b06932da6faaa7fc70d5b5d2f4', '2010-03-02 03:57:08', 2),
(12, 1, 'bernardo', 'e10adc3949ba59abbe56e057f20f883e', '2010-03-02 03:59:31', 1),
(14, 1, 'Sara', '4733a44073c81970cccbca6e1ede188b', '2010-05-22 05:37:34', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vereda`
--

CREATE TABLE IF NOT EXISTS `vereda` (
  `id_vereda` int(11) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  `id_corregimiento` int(11) NOT NULL,
  PRIMARY KEY  (`id_vereda`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `vereda`
--

INSERT INTO `vereda` (`id_vereda`, `nombre`, `id_corregimiento`) VALUES
(1, 'No aplica', 0),
(2, 'prueba', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_informacion_usuario`
--
CREATE TABLE IF NOT EXISTS `vista_informacion_usuario` (
`nombreUsuario` varchar(100)
,`fecha_creacion` datetime
,`id` int(11)
,`grupo` varchar(100)
,`id_grupo` int(11)
,`estado` tinyint(4)
,`nombre` varchar(100)
,`apellido` varchar(100)
,`cedula` int(11)
,`sexo` char(1)
,`tipo_sangre` varchar(5)
,`fecha_nacimiento` date
,`estado_civil` varchar(20)
,`telefono` int(11)
,`direccion` varchar(250)
,`celular` int(11)
,`email` varchar(100)
,`departamento` varchar(200)
,`id_departamento` int(11)
,`ciudad` varchar(200)
,`id_ciudad` int(11)
,`contrato` int(11)
,`nivel_academico` varchar(50)
,`etnia` varchar(50)
,`estrato` int(11)
,`foto` varchar(100)
,`barrio` varchar(250)
,`vereda` varchar(100)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zona`
--

CREATE TABLE IF NOT EXISTS `zona` (
  `id_zona` int(11) NOT NULL auto_increment,
  `nombre` varchar(20) NOT NULL,
  PRIMARY KEY  (`id_zona`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `zona`
--


-- --------------------------------------------------------

--
-- Estructura para la vista `vista_informacion_usuario`
--
DROP TABLE IF EXISTS `vista_informacion_usuario`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `guardacivico`.`vista_informacion_usuario` AS select `u`.`nombre` AS `nombreUsuario`,`u`.`fecha_creacion` AS `fecha_creacion`,`u`.`id` AS `id`,`g`.`nombre` AS `grupo`,`g`.`id_grupo` AS `id_grupo`,`u`.`estado` AS `estado`,`p`.`nombre` AS `nombre`,`p`.`apellido` AS `apellido`,`p`.`cedula` AS `cedula`,`p`.`sexo` AS `sexo`,`p`.`tipo_sangre` AS `tipo_sangre`,`p`.`fecha_nacimiento` AS `fecha_nacimiento`,`p`.`estado_civil` AS `estado_civil`,`p`.`telefono` AS `telefono`,`p`.`direccion` AS `direccion`,`p`.`celular` AS `celular`,`p`.`email` AS `email`,`d`.`nombre` AS `departamento`,`d`.`id_departamento` AS `id_departamento`,`c`.`nombre` AS `ciudad`,`c`.`id_ciudad` AS `id_ciudad`,`p`.`contrato` AS `contrato`,`p`.`nivel_academico` AS `nivel_academico`,`e`.`nombre` AS `etnia`,`p`.`estrato` AS `estrato`,`p`.`foto` AS `foto`,`b`.`nombre` AS `barrio`,`v`.`nombre` AS `vereda` from (((((((`guardacivico`.`usuario` `u` join `guardacivico`.`grupos` `g` on((`u`.`id_grupo` = `g`.`id_grupo`))) join `guardacivico`.`persona` `p` on((`p`.`id_usuario` = `u`.`id`))) join `guardacivico`.`departamento` `d` on((`d`.`id_departamento` = `p`.`id_departamento`))) join `guardacivico`.`ciudad` `c` on((`c`.`id_ciudad` = `p`.`id_ciudad`))) join `guardacivico`.`etnia` `e` on((`e`.`id_etnia` = `p`.`etnia`))) join `guardacivico`.`barrio` `b` on((`b`.`id_barrio` = `p`.`id_barrio`))) join `guardacivico`.`vereda` `v` on((`v`.`id_vereda` = `p`.`id_vereda`)));
